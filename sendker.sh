#!/bin/sh

SERIAL_PORT=$(ls -t -U /dev/cu.serial-*)
#SERIAL_PORT="/dev/cu.KeySerial1"
#SERIAL_PORT="/dev/cu.USA19H142P1.1"
#SERIAL_PORT="/dev/cu.usbserial-A96PPB33"
BAUDRATE=38400

stty -f $SERIAL_PORT $BAUDRATE,crtscts,-ixon,raw,-hup
lsz --ymodem --1k kermit.elf > $SERIAL_PORT < $SERIAL_PORT
#lsz --ymodem ozs?-*.* > $SERIAL_PORT < $SERIAL_PORT
