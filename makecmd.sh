#!/bin/sh

rm -f *.bin *.elf *.map *.err
mpm -bG -I../../oz/def kermit.asm
if test $? -eq 0; then
    # program compiled successfully, apply leading Z80 ELF header
    mpm -bG -nMap -I../../oz/def -okermit.elf kermit-elf.asm
fi
