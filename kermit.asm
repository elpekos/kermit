; -----------------------------------------------------------------------------
;
; Kermit-88
;
; (C) Wally Wright, 1990
; (C) Thierry Peycru, 2022
;
; Source code was reverse engineered from version 1.04 (04-Nov-90)
; using DZASM utility
;
; -----------------------------------------------------------------------------

module kermit88

include "stdio.def"
include "fileio.def"
include "director.def"
include "dor.def"
include "syspar.def"
include "saverst.def"
include "integer.def"
include "serintfc.def"
include "time.def"
include "char.def"
include "error.def"
include "bastoken.def"

include "kermit.inc"

IF BASIC
;
;       BBC Basic wrapper
;
        org     $2300                           ; default basic program area
.line_org
        defb    line_end - line_org             ; line length
        defw    1                               ; line number (zero in orginal version, verbose LIST)
        defm    BAS_IF, BAS_PAGE_G, "=&2300 ", BAS_LOMEM_P, "=24046:"
        defm    BAS_CALL, "&2320 ", BAS_ELSE, BAS_PRINT, '"', "PAGE!", '"'
        defb    CR                              ; end of line
.line_end
        defb    0                               ; program terminator
        defb    $FF,$FF,$FF,$FF,$FF,$FF         ; padding

.basic_entry                                    ; at $2320
        ld      hl, 0
        add     hl, sp
        ld      (ws_BasStk), hl                 ; save basic stack
        ld      hl, ($1FFE)                     ; use system stack
        ld      (ws_KerStk), hl
        ld      sp, hl
        call    kermit_entry
        ld      hl, (ws_BasStk)
        ld      sp, hl                          ; restore basic stack
        ret
ELSE
;
;       elf command
;
        org     EXEC_ORG
.elf_entry
        ld      hl, 0
        add     hl, sp
        ld      (ws_KerStk), hl
        jp      kermit_entry
ENDIF

; -----------------------------------------------------------------------------
;
;       CPSMIT.ASM
;
;       This file contains the system-independent initialization, the main
;       loop, and the commands that don't have any place better to go.
;       All the SET xxx and status routines now in CPSCOM.ASM as CPSMIT.ASM
;       was getting too big.
;
mitver:
        defm    "Z88MIT (01) 30-Oct-90", $00

kermit_entry:
        xor     a
        ld      b,a
        ld      hl,errhan
        oz      OS_Erh
        ld      (L_58F8),a
        ld      (L_58F6),hl
        call    ClsAppNameVersion
        ld      hl, kerdc_nam
        oz      Dc_nam
        ld      de,nq_txbaud
        ld      bc, PA_Txb
        ld      a,$02
        oz      OS_Nq
        jp      c,errorbox
        ld      hl,(nq_txbaud)
        ld      (ws_baudrate),hl
        ld      de,nq_rxbaud
        ld      bc, PA_Rxb
        ld      a,$02
        oz      OS_Nq
        jp      c,errorbox
        ld      de,nq_xonxoff
        ld      bc, PA_Xon
        ld      a,$01
        oz      OS_Nq
        jp      c,errorbox
        ld      c,$00
        ld      a,(nq_xonxoff)
        cp      $59
        jp      nz,L_4C44
        inc     c
L_4C44:
        ld      hl,L_5855
        ld      (hl),c
        ld      de,nq_parity
        ld      bc, PA_Par
        ld      a,$01
        oz      OS_Nq
        jp      c,errorbox
        ld      a,(nq_parity)
        ld      (parity),a
        oz      OS_Pout
        defm    CR,LF,"For help, type ? at any point in a command",0
        call    L_2668

;       This is the main KERMIT loop.
;       It prompts for and gets the users commands.

kermit:
        ld      hl,(ws_KerStk)
        ld      sp,hl
        call    L_4B25
        xor     a
        ld      (L_5BBE),a
        ld      (L_5BBC),a
        ;ld      de, L_4F8F (prompt)
        call    prompt
        ld      de,L_2396
        ld      hl,L_247C
        call    keycmd
        ex      de,hl
        jp      (hl)

;       here from: log, setcom, read, cfmcmd

kermt3:
        ld      de,L_4FD7
        call    prtstr
        jp      kermit

kerdc_nam:
        defm    "Kermit", $00

L_2396:
        defb    $1A
        defm    $05, "BREAK", $00
        defw    cmd_break
        defm    $03, "BYE", $00
        defw    unimplemented
        defm    $01, "C", $00
        defw    cmd_connect
        defm    $02, "CD", $00
        defw    cmd_cd
        defm    $07, "CONNECT", $00
        defw    cmd_connect
        defm    $03, "CWD", $00
        defw    cmd_cd
        defm    $09, "DIRECTORY", $00
        defw    cmd_directory
        defm    $04, "EXIT", $00
        defw    cmd_exit
        defm    $06, "FINISH", $00
        defw    unimplemented
        defm    $03, "GET", $00
        defw    cmd_get
        defm    $04, "HELP", $00
        defw    cmd_help
        defm    $05, "INPUT", $00
        defw    cmd_input
        defm    $03, "LOG", $00
        defw    cmd_log
        defm    $06, "LOGOUT", $00
        defw    unimplemented
        defm    $05, "PAUSE", $00
        defw    cmd_pause
        defm    $07, "RECEIVE", $00
        defw    cmd_receive
        defm    $06, "REMOTE", $00
        defw    cmd_remote
        defm    $04, "SEND", $00
        defw    cmd_send
        defm    $03, "SET", $00
        defw    cmd_set
        defm    $04, "SHOW", $00
        defw    cmd_show
        defm    $06, "STATUS", $00
        defw    cmd_show
        defm    $06, "STRING", $00
        defw    cmd_string
        defm    $04, "TAKE", $00
        defw    cmd_take
        defm    $08, "TRANSMIT", $00
        defw    cmd_transmit
        defm    $04, "TYPE", $00
        defw    cmd_type
        defm    $07,"VERSION", $00
        defw    cmd_version
L_247C:
        defm    $01, "T", $0d, $0a, "BREAK", $01, "2X2EXIT"
        defm    $01, "2XDLOG", $01, "2XVSEND", $01, "2XhTAKE"
        defm    $0d, $0a, "BYE", $01, "2X2FINISH", $01, "2XDLOGOUT"
        defm    $01, "2XVSET", $01, "2XhTRANSMIT"
        defm    $0d, $0a, "CONNECT", $01, "2X2GET"
        defm    $01, "2XDPAUSE", $01, "2XVSHOW"
        defm    $01, "2XhTYPE", $0d, $0a, "CD/CWD"
        defm    $01, "2X2HELP", $01, "2XDRECEIVE"
        defm    $01, "2XVSTATUS", $01, "2XhVERSION"
        defm    $0d, $0a, "DIRECTORY", $01, "2X2INPUT"
        defm    $01, "2XDREMOTE", $01, "2XVSTRING"
        defm    $01, "T", $00

unimplemented:
        oz      OS_Pout
        defm    " (Not implemented)",CR,LF,0
        jp      kermit


;
;       This is the BREAK command.  It sends a 'B' to the system dependent
;       interrupt routines (test for escape-cokebottle xxx) and do a break
;       if the overlay can.  Else, we tell user not to be so silly.
;
cmd_break:
        call    cfmcmd
        ld      a, 'B'
        call    sysint
        jp      kermit

;
;       This is the EXIT command.  It leaves KERMIT and returns to CP/M.
;       alternate entries: exit1, from BYE command;
;       exit2, from initialization (if it fails)
;
cmd_exit:
        call    cfmcmd
        ld      hl,nq_txbaud
        call    sp_txbaud
        ld      hl,nq_rxbaud
        call    sp_rxbaud
        ld      hl,nq_xonxoff
        call    sp_xonxoff
        ld      hl,nq_parity
        call    sp_parity
        call    si_softreset
        ld      a,(takopn)
        or      a
        call    nz,L_4B3C
        ld      a,(logflg)
        and     $80
        call    nz,L_41C6
        oz      GN_Nln
L_2586:
        ld      hl,(L_58F6)
        ld      a,(L_58F8)
        ld      b,$00
        oz      OS_Erh
        ; ld hl, (ws_stk)
        ; ld sp, hl
        ret                                     ; return to shell

;
;       Input command.  Syntax:
;               INPUT [Wait period] [string]
;       where
;               Wait period is a time period to wat for
;               string is a string to expect back from the host.  Control
;                       characters are entered as \ and an octal number.
;
;       I can see uses for this command from other routines...
;
cmd_input:
        ld      a,$06
        call    commnd
        jp      kermit

L_259D:
        ld      hl,(number)
        ld      (L_5A65),hl
        ld      (L_5A67),hl
        ld      de, stbuff
        ld      a,$05
        call    commnd
        jp      kermit

L_25B1:                                         ; !!!
        ld      (strcnt),a
        call    cfmcmd

L_25B7:
        xor     a
        ld      (L_5961),a
L_25BB:
        ld      bc,$0064
        oz      Os_dly
        ld      hl,(L_5A65)
        dec     hl
        ld      (L_5A65),hl
        ld      a,h
        or      l
        jp      z,L_261E
        call    si_getbyte
        and     $7F
        jp      nz,L_25F4
        call    inpcon
        and     $7F
        jp      nz,L_25E7
        ld      a,(strcnt)
        and     a
        jp      nz,L_25BB
        jp      kermit
L_25E7:
        cp      $03
        jp      z,kermit
        cp      $1A
        jp      nz,kermit
        jp      L_25BB
L_25F4:
        ld      e,a
        ld      a,(L_5961)
        ld      hl,stbuff
        add     a,l
        ld      l,a
        ld      a,$00
        adc     a,h
        ld      h,a
        ld      a,e
        cp      (hl)
        jp      nz,L_25B7
        ld      a,(L_5961)
        inc     a
        ld      (L_5961),a
        ld      hl,(L_5A67)
        ld      (L_5A65),hl
        ld      e,a
        ld      a,(strcnt)
        sub     e
        jp      nz,L_25BB
        jp      kermit
L_261E:
        oz      OS_Pout
        defm    CR,LF,"Failed to receive input string in time",0
        jp      kermit

;
;       This is the HELP command.  It gives a list of the commands.
;
cmd_help:
        call    cfmcmd
        ld      de,L_247C
        call    prtstr
        jp      kermit

;
;       This is the LOG command.  It logs a session to a file.
;
cmd_log:
        ld      a,$05
        ld      de,logfln
        call    commnd
        jp      kermt3

L_263E:
        ex      de,hl
        ld      (hl),$00
        call    cfmcmd
        ld      a,$01
        ld      (logflg),a
        jp      kermit

;
;       PAUSE [Wait period]. Just wait for a couple of tics...
;
cmd_pause:
        ld      a,$06
        call    commnd
        jp      kermit

L_2654:
        call    cfmcmd
        ld      hl,(number)
        ld      de,$0064
        oz      Gn_m16
        push    hl
        pop     bc
        oz      Os_dly
        jp      kermit

L_2668:
        ld      a,$02
        ld      (L_5955),a
        ld      hl,L_5956
        jp      L_2693

;
;       This is the TAKE command.  It take input from a file.
;       TAKE1 is the entry for automatically TAKE-ing KERMIT.INI (or whatever
;       the file name at taknam is) from the default drive
;       [18] code added to accept command tails.  See note [18] above
;
cmd_take:
        ld      a,(takopn)
        or      a
        jp      nz,L_26C5
        ld      a,$01
        ld      (L_5955),a
        ld      a,$05
        ld      de,flnbuf
        call    commnd
        jp      kermit

L_268A:
        ex      de,hl
        ld      (hl), 0
        call    cfmcmd
        ld      hl, flnbuf

L_2693:
        ld      de,buffer128
        ld      bc,$0012
        ld      a, OP_IN
        oz      GN_Opf
        jp      c,L_26AE
        ld      (hndtak),ix
        ld      a,(L_5955)
        ld      (takopn),a
        jp      kermit
L_26AE:
        ld      c,a
        ld      hl,L_5955
        ld      b,(hl)
        xor     a
        ld      (hl),a
        ld      a,c
        cp      $12
        jp      nz,errorbox
        ld      a,b
        and     $01
        jp      z,kermit
        ld      a,c
        jp      errorbox
L_26C5:
        oz      OS_Pout
        defm    CR,LF,"Cannot nest TAKE files",0
        call    L_4B3C
        jp      kermit

; -----------------------------------------------------------------------------
;
;       CPSCOM.ASM
;
;       This file contains some of the main loop commands, all SET xxx and
;       status routines.  File split from CPSMIT.ASM as that file 
;       was getting too big.
;
comver:
        defm    "Z88COM (03) 04-Nov-90", $00

cmd_set:
        ld      de,settab
        ld      hl,sethlp
        call    keycmd   
        ex      de,hl
        jp      (hl)

settab:
        defb    $10
        defm    $0b, "AUTORECEIVE", $00
        defw    set_autoreceive
        defm    $09, "BAUD-RATE", $00
        defw    set_baudrate
        defm    $10, "BLOCK-CHECK-TYPE", $00
        defw    set_blockcheck
        defm    $05, "DEBUG", $00
        defw    set_debug
        defm    $06, "ESCAPE", $00
        defw    set_escape
        defm    $09, "FILE-MODE", $00
        defw    set_filemode
        defm    $0c, "FLOW-CONTROL", $00
        defw    set_flowcontrol
        defm    $0a, "LOCAL-ECHO", $00
        defw    set_localecho
        defm    $07, "LOGGING", $00
        defw    set_logging
        defm    $06, "PARITY", $00
        defw    set_parity
        defm    $07, "RECEIVE", $00
        defw    set_receive
        defm    $04, "SEND", $00
        defw    set_send
        defm    $07, "TACTRAP", $00
        defw    set_tactrap
        defm    $08, "TERMINAL", $00
        defw    set_terminal
        defm    $05, "TIMER", $00
        defw    set_timer
        defm    $07, "WARNING", $00
        defw    set_warning
sethlp:
        defm    $01, "T", $0d, $0a, "AUTORECEIVE", $01, "2X2ESCAPE"
        defm    $01, "2XDLOGGING", $01, "2XVTACTRAP"
        defm    $0d, $0a, "BAUD-RATE", $01, "2X2FILE-MODE"
        defm    $01, "2XDPARITY", $01, "2XVTERMINAL"
        defm    $0d, $0a, "BLOCK-CHECK-TYPE"
        defm    $01, "2X2FLOW-CONTROL"
        defm    $01, "2XDRECEIVE", $01, "2XVTIMER"
        defm    $0d, $0a, "DEBUG", $01, "2X2LOCAL-ECHO"
        defm    $01, "2XDSEND", $01, "2XVWARNING"
        defm    $01, "T", $00

set_autoreceive:
        call    L_2BD8
        ld      (L_5BB9),a
        jp      kermit
set_blockcheck:
        ld      de,L_288A
        ld      hl,L_28D4
        call    L_2BDE
        ld      (L_5C33),a
        jp      kermit

L_288A:
        defb    $03
        defm    $14, "1-CHARACTER-CHECKSUM", $00
        defm    "11"                            ; !!
        defm    $14, "2-CHARACTER-CHECKSUM", $00
        defm    "22"                            ; !!
        defm    $15, "3-CHARACTER-CRC-CCITT", $00
        defm    "33"                            ; !!
L_28D4:
        defm    $0d, $0a, $01, "T1-CHARACTER-CHECKSUM"
        defm    $0d, $0a, "2-CHARACTER-CHECKSUM"
        defm    $0d, $0a, "3-CHARACTER-CRC-CCITT"
        defm    $01, "T", $00

set_send:
        ld      de,L_2927
        ld      hl,L_2952
        call    keycmd
        ex      de,hl
        jp      (hl)

L_2927:
        defb    $03
        defm    $08, "PAD-CHAR", $00
        defw    setsend_padchar
        defm    $07, "PADDING", $00
        defw    setsend_padding 
        defm    $0f, "START-OF-PACKET", $00
        defw    setsend_startofpacket
L_2952:
        defm    $0d, $0a, $01, "TPAD-CHAR    PADDING    START-OF-PACKET"
        defm    $01, "T", $00

setsend_startofpacket:
        call    cfmcmd
        ld      de,L_54D1
        call    prtstr
        call    L_4E9B
        ld      (L_58FB),a
        jp      kermit
setsend_padding:
        call    L_2A0A
        ld      (L_5C2B),a
        jp      kermit
setsend_padchar:
        call    L_29FD
        ld      (L_5C2D),a
        jp      kermit
set_receive:
        ld      de,L_29AE
        ld      hl,L_2952
        call    keycmd
        ex      de,hl
        jp      (hl)

L_29AE:
        defb    $03
        defm    $08, "PAD-CHAR", $00
        defw    setreceive_padchar
        defm    $07, "PADDING", $00
        defw    setreceive_padding
        defm    $0f, "START-OF-PACKET", $00
        defw    setreceive_startofpacket
setreceive_startofpacket:
        call    cfmcmd
        ld      de,L_54D1
        call    prtstr
        call    L_4E9B
        ld      (L_58FA),a
        jp      kermit
setreceive_padding:
        call    L_2A0A
        ld      (L_5C2C),a
        jp      kermit
setreceive_padchar:
        call    L_29FD
        ld      (L_5C2E),a
        jp      kermit
L_29FD:
        call    cfmcmd
        oz      OS_Pout
        defm    CR,LF,"Type the new padding character: ",0
        call    L_4E9B
        ret
L_2A0A:
        ld      a,$06
        call    commnd
        jp      kermit
L_2A12:
        call    cfmcmd
        ld      hl,(number)
        ld      a,l
        ret
set_logging:
        call    L_2BD8
        ld      (logflg),a
        jp      kermit
set_escape:
        call    cfmcmd
        oz      OS_Pout
        defm    CR,LF,"Type the new escape character:  ",0
        call    L_4E9B
        ld      (escchr),a
        jp      kermit
set_localecho:
        call    L_2BD8
        ld      (L_5C23),a
        jp      kermit
set_debug:
        call    L_2BD8
        ld      (dbgflg),a
        jp      kermit
set_timer:
        call    L_2BD8
        ld      (timflg),a
        jp      kermit
set_warning:
        call    L_2BD8
        ld      (L_5C24),a
        jp      kermit
set_flowcontrol:
        call    L_2BD8
        ld      (L_5855),a
        ld      c, 'N'
        ld      hl,L_5BBA
        or      a
        jp      z,sfc_0
        ld      c, 'Y'
sfc_0:
        ld      (hl),c
        call    sp_xonxoff
        call    si_softreset
        jp      kermit
set_filemode:
        ld      de,L_2A83
        ld      hl,L_2A97
        call    L_2BDE
        ld      (L_5C25),a
        jp      kermit

L_2A83:
        defb    $02
        defm    $05, "ASCII", $00
        defm    $01, $01
        defm    $06, "BINARY", $00
        defm    $02, $02
L_2A97:
        defm    $0d, $0a, $01, "TASCII      BINARY"
        defm    $01, "T", $00

set_parity:
        ld      de,L_2AC5
        ld      hl,L_2AEE
        call    L_2BDE
        ld      hl,parity
        ld      (hl),a
        call    sp_parity
        call    si_softreset
        jp      kermit

L_2AC5:
        defb    $05
        defm    $04, "EVEN", $00
        defm    "EE"
        defm    $04, "MARK", $00
        defm    "MM"
        defm    $04, "NONE", $00
        defm    "NN"
        defm    $03, "ODD", $00
        defm    "OO"
        defm    $05, "SPACE", $00
        defm    "SS"
L_2AEE:
        defm    $0d, $0a, $01, "TEVEN", $09, "MARK", $09, "NONE"
        defm    $09, "ODD", $09, "SPACE", $01, "T", $00

set_tactrap:
        ld      de,L_2B34
        ld      hl,L_2B4F
        call    L_2BDE
        or      a
        jp      z,L_2B2E
        cp      $01
        jp      z,L_2B2B
        oz      OS_Pout
        defm    CR,LF,"Type the new TAC intercept character:  ",0
        call    L_4E9B
        ld      (L_5C35),a
L_2B2B:
        ld      a,(L_5C35)
L_2B2E:
        ld      (L_5C34),a
        jp      kermit

L_2B34:
        defb    $03
        defm    $09, "CHARACTER", $00
        defm    $02, $02
        defm    $03, "OFF", $00
        defm    $00, $00
        defm    $02, "ON", $00
        defm    $01, $01
L_2B4F:
        defm    $0d, $0a, $01, "TON    OFF    CHARACTER"
        defm    $01, "T", $00

set_terminal:
        ld      de,L_2B7C
        ld      hl,L_2B8D
        call    L_2BDE
        ld      a,d
        ld      (ws_terminal),a
        jp      kermit

L_2B7C:
        defb    $02
        defm    $04, "DUMB", $00
        defm    $02, $02
        defm    $04, "VT52", $00
        defm    $01, $01
L_2B8D:
        defm    $0d, $0a, $01, "TDUMB      VT52"
        defm    $01, "T", $00

set_baudrate:
        ld      de,L_4DD2
        ld      hl,L_4E11
        call    keycmd
        push    de
        call    cfmcmd
        pop     hl
        ld      (ws_baudrate),hl
        call    L_4E58
        jp      kermit

L_2BB9:
        defb    $02
        defm    $03, "OFF", $00
        defm    $00, $00
        defm    $02, "ON", $00
        defm    $01, $01
L_2BC7:
        defm    $0d, $0a, $01, "TOFF     ON", $01, "T", $00

L_2BD8:
        ld      de,L_2BB9
        ld      hl,L_2BC7
L_2BDE:
        call    keycmd
        ld      (L_5BBA),a
        call    cfmcmd
        ld      a,(L_5BBA)
        ret

;
;       This is the SHOW command.
;
cmd_show:
        call    cfmcmd
        call    ClearScreen
        ld      de,L_55DC
        call    prtstr
        call    L_2C03
        ld      bc,$0701
        call    CursorAtBC
        jp      kermit
L_2C03:
        call    L_2C3A                          
        call    L_2C62
        call    L_2C91
        call    L_2C9D
        call    L_2CA9
        call    L_2CC0
        call    L_2CCC
        call    L_2CD8
        call    L_2CFC
        call    L_2D08
        call    L_2D14
        call    L_2D20
        call    L_2D42
        call    L_2D50
        call    L_2D5E
        call    L_2D6A
        call    L_2D8B
        call    L_2D97
        ret
L_2C3A:
        ld      bc,$020D
        call    CursorAtBC
        ld      a,(L_5C21)
        ld      hl,L_4DD2
        ld      d,(hl)
        inc     hl
L_2C48:
        ld      b,(hl)
        inc     b
        inc     hl
L_2C4B:
        inc     hl
        dec     b
        jp      nz,L_2C4B
        ld      c,(hl)
        inc     hl
        ld      b,(hl)
        cp      b
        jp      z,L_2D76
        inc     hl
        dec     d
        jp      nz,L_2C48
        ld      hl,L_5727
        jp      prtstr
L_2C62:
        ld      bc,$030D
        call    CursorAtBC
        ld      a,(parity)
        ld      de,L_5709
        cp      $4E
        jp      z,prtstr
        ld      de,L_570F
        cp      $4D
        jp      z,prtstr
        ld      de,L_5715
        cp      $53
        jp      z,prtstr
        ld      de,L_571B
        cp      $4F
        jp      z,prtstr
        ld      de,L_5721
        jp      prtstr
L_2C91:
        ld      bc,$040F
        call    CursorAtBC
        ld      a,(L_5855)
        jp      staton
L_2C9D:
        ld      bc,$050F
        call    CursorAtBC
        ld      a,(L_5C23)
        jp      staton
L_2CA9:
        ld      bc,$060C
        call    CursorAtBC
        ld      a,(L_5C25)
        ld      de,L_56E8
        cp      $01
        jp      z,prtstr
        ld      de,L_56EF
        jp      prtstr
L_2CC0:
        ld      bc,$070F
        call    CursorAtBC
        ld      a,(L_5C24)
        jp      staton
L_2CCC:
        ld      bc,$0227
        call    CursorAtBC
        ld      a,(timflg)
        jp      staton
L_2CD8:
        ld      bc,$0326
        call    CursorAtBC
        ld      a,(ws_terminal)
        ld      c,a
        ld      hl,L_2B7C
        ld      b,(hl)
L_2CE6:
        inc     hl
        ld      e,(hl)
        ld      d,$00
        inc     hl
        ex      de,hl
        add     hl,de
        inc     hl
        inc     hl
        cp      (hl)
        jp      z,L_2CF8
        dec     b
        ret     z
        jp      L_2CE6
L_2CF8:
        call    prtstr
        ret
L_2CFC:
        ld      bc,$0429
        call    CursorAtBC
        ld      a,(L_5C33)
        oz      OS_Out
        ret
L_2D08:
        ld      bc,$0527
        call    CursorAtBC
        ld      a,(dbgflg)
        jp      staton
L_2D14:
        ld      bc,$0628
        call    CursorAtBC
        ld      a,(escchr)
        jp      L_2DA0
L_2D20:
        ld      bc,$0724
        call    CursorAtBC
        ld      de,logfln
        call    prtstr
        oz      OS_Pout
        defm    1,"T IS ",1,"T",0
        ld      a,(logflg)
        and     $7F
        cp      $02
        jp      nz,staton
        oz      OS_Pout
        defm    "suspended",0
        jp      prtstr
L_2D42:
        ld      bc,$0344
        call    CursorAtBC
        ld      a,(L_58FB)
        add     a,$40
        oz      OS_Out
        ret
L_2D50:
        ld      bc,$034E
        call    CursorAtBC
        ld      a,(L_58FA)
        add     a,$40
        oz      OS_Out
        ret
L_2D5E:
        ld      bc,$0440
        call    CursorAtBC
        ld      a,(L_5C2B)
        jp      L_2D73
L_2D6A:
        ld      bc,$044A
        call    CursorAtBC
        ld      a,(L_5C2C)
L_2D73:
        ld      c,a
        ld      b,$00
L_2D76:
        ld      a,$50
        ld      hl,$0002
        ld      de,buffer128
        oz      Gn_pdn
        ex      de,hl
        ld      (hl),$00
        ld      hl,buffer128
        oz      GN_Sop
        ret
L_2D8B:
        ld      bc,$0543
        call    CursorAtBC
        ld      a,(L_5C2D)
        jp      L_2DA0
L_2D97:
        ld      bc,$054D
        call    CursorAtBC
        ld      a,(L_5C2E)
L_2DA0:
        push    af
        cp      $20
        jp      p,L_2DB4
        ld      a,$01
        oz      OS_Out
        ld      a,$2B
        oz      OS_Out
        pop     af
        or      $40
        jp      L_2DB9
L_2DB4:
        ld      a,$20
        oz      OS_Out
        pop     af
L_2DB9:
        oz      OS_Out
        ret
cmd_version:
        call    cfmcmd
        oz      GN_Nln
        ld      hl,vertab
L_2DC5:
        ld      e,(hl)
        inc     hl
        ld      d,(hl)
        inc     hl
        ld      a,d
        or      e
        jp      z,kermit
        push    hl
        call    prtstr
        call    L_4B87
        pop     hl
        jp      nc,L_2DC5
        jp      kermit

vertab:
        defw    mitver
        defw    comver
        defw    pk1ver
        defw    pk2ver
        defw    remver
        defw    ttver 
        defw    osver 
        defw    cmdver
        defw    utlver
        defw    sysver
        defw    datver
        defw    0
L_2DF4:
        ld      bc,$0000
        call    CursorAtBC
        ld      de,L_56E4
        ld      a,(L_5C34)
        or      a
L_2E01:
        jp      z,prtstr
        oz      OS_Out
        jp      L_4B87

;
;       Display current state of a boolean flag.
;       called with A/ value (zero = Off, non-zero = On)
;
staton:
        ld      de,L_56E0
        or      a
        jp      nz,prtstr
        ld      de,L_56E4
        jp      prtstr

;
;       STRING command
;       get a string from the user and send it to the host.
;
cmd_string:
        ld      a,$05
        ld      de,stbuff
        call    commnd
        jp      kermit
L_2E21:
        ld      (strcnt),a
        call    cfmcmd
        ld      a,(strcnt)
        and     a
        jp      z,kermit
        ld      de,stbuff
L_2E31:
        ld      a,(de)
        inc     de
        push    de
        ld      e,a
        call    si_putbyte
        pop     de
        ld      a,(strcnt)
        dec     a
        ld      (strcnt),a
        jp      nz,L_2E31
        jp      kermit

; -----------------------------------------------------------------------------
;
;       CPSPK1.ASM
;
;       This file contains the (system-independent) routines that implement
;       the KERMIT protocol, and the commands that use them:
;       RECEIVE, SEND, FINISH, and LOGOUT.
;
pk1ver:
        defm    "Z88PK1 (04) 04-Nov-90", $00

;
;       GET command   [gnn]
;       here from: kermit
;
cmd_get:        ; read
        ld      a,$FF
        ld      (L_5BBE),a                      ; getrxflg !! this flag is never read/tested !!
        ld      de,remdat
        ld      a,$05                           ; cmtxt
        call    commnd
        jp      kermt3
L_2E6C:
        or      a
        jp      z,kermt3
        ld      (L_5BC0),a
        ex      de,hl
        ld      (hl),$00
        call    trnscr
        jp      L_2E84

;
; enter here for RECEIVE command  [gnn]
;
cmd_receive:    ; read0
        ld      a,$00
        ld      (L_5BC0),a
        ld      (L_5BBE),a
L_2E84:
        call    trnscr
        ld      de,remnam
        ld      a,$05
        call    commnd
        jp      kermt3
L_2E92:
        ld      (L_5C1B),a
        ld      (L_5BBE),a
        ex      de,hl
        xor     a
        ld      (hl),a
        ld      a,(L_5BC0)
        or      a
        jp      z,L_2EB3
        ld      a,$31
        ld      (curchk),a
        xor     a
        ld      (argblk),a
        ld      a,$52
        call    spack
        jp      kermt3
L_2EB3:
        xor     a
        ld      (czseen),a
        ld      hl,$0000
        ld      (numpkt),hl
        ld      (numrtr),hl
        ld      (pktnum),a
        ld      (numtry),a
        call    L_4C98
        ld      hl,$0000
        call    prtnum
        ld      a,$52
        ld      (state),a
L_2ED4:
        call    L_4C92
        ld      hl,(numpkt)
        call    prtnum
        ld      a,(state)
        cp      $44
        jp      nz,L_2EEB
        call    rdata
        jp      L_2ED4
L_2EEB:
        cp      $58
        jp      nz,L_2EF6
        call    rfile
        jp      L_2ED4
L_2EF6:
        cp      $46
        jp      nz,L_2F01
        call    rfile
        jp      L_2ED4
L_2F01:
        cp      $52
        jp      nz,L_2F1A
        call    rinit
        ld      a,(state)
        cp      $46
        jp      nz,L_2ED4
        ld      de,L_53AD
        call    finmes
        jp      L_2ED4
L_2F1A:
        cp      $43
        jp      nz,L_2F55
        ld      de,L_5199
        ld      a,(czseen)
        or      a
        jp      z,L_2F33
        call    L_4B16
        xor     a
        ld      (czseen),a
        ld      de,L_525A
L_2F33:
        call    finmes
        ld      a,(L_5BB9)
        and     a
        jp      z,L_2F52
        oz      OS_Pout
        defm    CR,LF,"* Press any key to continue *",0
L_2F43:
        call    si_getbyte
        and     a
        jp      nz,L_2EB3
        call    inpcon
        and     $7F
        jp      z,L_2F43
L_2F52:
        jp      kermit
L_2F55:
        cp      $41
        jp      nz,L_2F5A
L_2F5A:
        ld      de,infms4
        call    finmes   
        jp      kermit   

;       Receive routines
;       ----------------

;
;       Receive init
;       called by: read
;
rinit:
        ld      a,(numtry)
        cp      $10                             ; imxtry
        jp      m,L_2F74
        ld      de,L_4FE7
        call    error3
        jp      abort
L_2F74:
        inc     a
        ld      (numtry),a
        ld      a,$31
        ld      (curchk),a
        call    rpack
        jp      sdnak_
L_2F83:
        cp      'S'
        jp      nz,L_2FBE
        ld      a,(numtry)
        ld      (oldtry),a
        xor     a
        ld      (numtry),a
        ld      a,(argblk)
        call    countp
        ld      a,(L_5BC0)
        ld      hl,remdat
        call    L_3625
        ld      hl,remdat
        call    L_35E4
        ld      (L_5BC0),a
        ld      a, 'Y'
        call    spack
        jp      abort
L_2FB2:
        ld      a,(L_5A84)
        ld      (curchk),a
        ld      a, 'F'
        ld      (state),a
        ret
L_2FBE:
        cp      'E'
        jp      nz,sdnak0
        call    error_
        jp      abort

;
;       Receive file
;       called by: read
;
rfile:
        ld      a,(numtry)
        cp      5
        jp      m,L_2FDA
        ld      de,L_5004                       ; unable to receive filename
        call    error3
        jp      abort
L_2FDA:
        inc     a
        ld      (numtry),a
        call    rpack
        jp      sdnak_
L_2FE4:
        cp      'S'
        jp      nz,L_3025
        ld      a,(oldtry)
        cp      $10
        jp      m,L_2FFA
        ld      de,L_4FE7                       ; unable to receive initiate
        call    error3
        jp      abort
L_2FFA:
        inc     a
        ld      (oldtry),a
        ld      a,(pktnum)
        dec     a
        and     $3F
        ld      b,a
        ld      a,(argblk)
        cp      b
        jp      nz,sdnak0
        call    updrtr
        xor     a
        ld      (numtry),a
        ld      hl,remdat
        call    L_35E4
        ld      (L_5BC0),a
        ld      a, 'Y'
        call    spack
        jp      abort
L_3024:
        ret
L_3025:
        cp      'Z'
        jp      nz,L_303F
        ld      a,(oldtry)
        cp      5
        jp      m,L_303B
        ld      de,L_5022
        call    error3
        jp      abort
L_303B:
        call    tryagn
        ret
L_303F:
        cp      'F'
        jp      nz,L_306C
        call    comppn
        jp      nz,sdnak0
        call    countp
        call    L_3889
        jp      abort
L_3053:
        ld      a,(numtry)
        ld      (oldtry),a
        call    L_3D92
        ld      a, 'D'
        ld      (state),a
        ld      a,(czseen)
        cp      'Z'
        ret     z
        xor     a
        ld      (czseen),a
        ret
L_306C:
        cp      'X'
        jp      nz,L_309C
        call    comppn
        jp      nz,sdnak0
        call    countp
        ld      de,remdat
        ld      a,(L_5BC0)
        and     a
        jp      z,L_3093
        ld      l,a
        ld      h, 0
        add     hl,de
        ld      (hl), 0
        ld      de,remdat
        call    prtstr
        oz      GN_Nln
L_3093:
        ld      a, 'D'
        ld      (state),a
        ld      (L_59E4),a
        ret
L_309C:
        cp      'B'
        jp      nz,L_30B9
        call    comppn
        jp      nz,sdnak0
        xor     a
        ld      (L_5BC0),a
        ld      a, 'Y'
        call    spack
        jp      abort
L_30B3:
        ld      a, 'C'
        ld      (state),a
        ret
L_30B9:
        cp      'E'
        jp      nz,abort
        call    error_
        jp      abort

;
;       Receive data
;       called by: read
;
rdata:
        ld      a,(numtry)
        cp      5
        jp      m,L_30D5
        ld      de,L_5042                       ; unable to receive data
        call    error3
        jp      abort
L_30D5:
        inc     a
        ld      (numtry),a
        call    rpack
        jp      sdnak_
L_30DF:
        cp      'D'
        jp      nz,L_3133
        call    comppn
        jp      z,L_30FF
        ld      a,(oldtry)
        cp      5
        jp      m,L_30FB
        ld      de,L_5042                       ; unable to receive data
        call    error3
        jp      abort
L_30FB:
        call    tryagn
        ret
L_30FF:
        call    countp
        ld      a,(numtry)
        ld      (oldtry),a
        ld      a,(L_5BC0)
        call    L_36BD
        jp      abort
L_3111:
        xor     a
        ld      (numtry), a
        ld      (L_5BC0), a
        ld      c, a
        ld      a, (czseen)
        or      a
        jp      z, L_312A
        ld      c, a
        ld      a, 1
        ld      (L_5BC0), a
        ld      a, c
        ld      (remdat), a
L_312A:
        ld      a, 'Y'
        call    spack
        jp      abort
L_3132:
        ret
L_3133:
        cp      'F'
        jp      nz,L_314D
        ld      a,(oldtry)
        cp      5
        jp      m,L_3149
        ld      de,L_5004
        call    error3
        jp      abort
L_3149:
        call    tryagn
        ret
L_314D:
        cp      'Z'
        jp      nz,L_318D
        call    comppn
        jp      nz,sdnak0
        call    countp
        ld      a,(L_5BC0)
        cp      1
        jp      nz,L_316B
        ld      a,(remdat)
        cp      'D'
        jp      z,L_3175
L_316B:
        call    L_4B00
        jp      L_3184
L_3171:
        xor     a
        ld      (czseen),a
L_3175:
        ld      a,(numtry)
        ld      (oldtry),a
        call    L_3D92
        ld      a, 'F'
        ld      (state),a
        ret
L_3184:
        ld      de,L_505B
        call    error3
        jp      abort
L_318D:
        cp      'E'
        jp      nz,abort
        call    error_
        jp      abort

;
;       SEND command
;       here from: kermit
;
cmd_send:
        ld      a, 5                            ; cmifi = cmtext
        ld      de, flnbuf
        call    commnd
        jp      kermit                          ; failed

;       section to get remote filename [gnn]

L_31A3:
        ex      de,hl                           ; success
        ld      (hl), 0
        ld      de, remnam                      ; remnam = remote filename
        ld      a, 5                            ; cmtext
        call    commnd
        jp      kermt3                          ; failed
L_31B1:
        ex      de,hl                           ; success
        ld      (hl), 0
        ld      (L_5C1B),a
        call    cfmcmd
L_31BA:
        call    wch_nx                          ; open wc / fetch next
        jp      c,errorbox
        cp      DN_FIL
        jp      nz,L_31BA
        call    trnscr                          ; transfer screen
        xor     a
        ld      (pktnum),a
        ld      (numtry),a
        ld      (wrn8bl),a                      ; 8bit lost warning
        ld      hl, 0
        ld      (numpkt),hl
        ld      (numrtr),hl
        call    L_4C98
        ld      hl, 0
        call    prtnum
        ld      a, '1' 
        ld      (curchk),a                      ; 1 char checksum
        ld      a, 'S'
        ld      (state),a                       ; initiate state

;SEND state table switcher

L_31EE:
        call    L_4C92                          ; Clear the line and initialize the buffers
        ld      hl,(numpkt)
        call    prtnum
        ld      a,(state)
        cp      $44
        jp      nz,L_3205
        call    L_33DA
        jp      L_31EE
L_3205:
        cp      $46
        jp      nz,L_3210
        call    L_3311
        jp      L_31EE
L_3210:
        cp      $5A
        jp      nz,L_321B
        call    L_34DA
        jp      L_31EE
L_321B:
        cp      $53
        jp      nz,L_3234
        call    L_326E
        ld      a,(state)
        cp      $46
        jp      nz,L_31EE
        ld      de,L_539B
        call    finmes
        jp      L_31EE
L_3234:
        cp      $42
        jp      nz,L_323F
        call    L_3579
        jp      L_31EE
L_323F:
        cp      $43
        jp      nz,L_3257
        ld      de,L_5199
        ld      a,(czseen)
        or      a
        jp      z,L_3251
        ld      de,L_525A
L_3251:
        call    finmes
        jp      kermit
L_3257:
        cp      $41
        jp      nz,L_3265
        ld      de,infms4
        call    finmes
        jp      kermit
L_3265:
        ld      de,infms4
        call    finmes
        jp      kermit
L_326E:
        ld      a,(numtry)
        cp      $10
        jp      m,L_327F
        ld      de,L_5065
        call    error3
        jp      abort
L_327F:
        inc     a
        ld      (numtry),a
        ld      a,$31
        ld      (curchk),a
        ld      a,(L_5C33)
        ld      (L_5A84),a
        ld      hl,remdat
        call    L_35E4
        ld      (L_5BC0),a
        ld      a,(numpkt)
        ld      (argblk),a
        ld      a,(state)
        call    spack
        jp      abort
L_32A6:
        call    rpack
        jp      r_____
L_32AC:
        cp      'Y'
        jp      nz,L_32E8
        call    comppn
        ret     nz
        call    countp
        ld      a,(L_5BC0)
        ld      hl,remdat
        call    L_3625
        ld      a,(numtry)
        ld      (oldtry),a
        xor     a
        ld      (numtry),a
        ld      a,(L_5A84)
        ld      (curchk),a
        ld      a,(state)
        cp      'I'
        jp      nz,L_32DF
        ld      a, 'X'
        ld      (state),a
        ret
L_32DF:
        ld      a, 'F'
        ld      (state),a
        call    getfil
        ret
L_32E8:
        cp      'N'
        jp      nz,L_3306
        call    updrtr
        ld      a,(pktnum)
        inc     a
        and     $3F
        ld      b,a
        ld      a,(argblk)
        cp      b
        ret     nz
        xor     a
        ld      (numtry),a
        ld      a, 'F'
        ld      (state),a
        ret
L_3306:
        cp      'E'
        jp      nz,abort
        call    error_
        jp      abort
L_3311:
        ld      a,(numtry)
        cp      5
        jp      m,L_3322
        ld      de,L_5065
        call    error3
        jp      abort
L_3322:
        inc     a
        ld      (numtry),a
        xor     a
        ld      (czseen),a
        call    L_4CA7
        ld      de,flnbuf
        call    prtstr
        ld      hl,remdat
        ld      a,(L_5C1B)
        or      a
        jp      nz,L_33C4
        ld      de,flnbuf
        call    L_49DA
L_3343:
        ld      (datptr),hl
        ld      (L_5BC0),a
        call    L_4C9E
        ld      de,remdat
        call    prtstr
        ld      a,(pktnum)
        ld      (argblk),a
        ld      a, 'F'
        call    spack
        jp      abort
L_3360:
        call    rpack
        jp      r_____
L_3366:
        cp      'Y'
        jp      nz,L_33A2
        call    comppn
        ret     nz
L_336F:
        call    countp
        ld      a,(numtry)
        ld      (oldtry),a
        xor     a
        ld      (numtry),a
        ld      (L_5A72),a
        ld      (L_5A73),a
        ld      (L_5A74),a
        ld      (L_5A75),a
        call    L_3777
        jp      L_3397
L_338E:
        ld      (L_5A82),a
        ld      a, 'D'
        ld      (state),a
        ret
L_3397:
        cp      -1
        jp      nz,abort
        ld      a, 'Z'
        ld      (state),a
        ret
L_33A2:
        cp      'N'
        jp      nz,L_33B9
        call    updrtr
        ld      a,(pktnum)
        inc     a
        and     $3F
        ld      b,a
        ld      a,(argblk)
        cp      b
        ret     nz
        jp      L_336F
L_33B9:
        cp      'E'
        jp      nz,abort
        call    error_
        jp      abort
L_33C4:
        ex      de,hl
        ld      hl,remnam
        ld      c,a
        ld      b,a
L_33CA:
        ld      a,(hl)
        ld      (de),a
        inc     hl
        inc     de
        dec     b
        ld      a,b
        or      a
        jp      nz,L_33CA
        ex      de,hl
        ld      (hl),a
        ld      a,c
        jp      L_3343
L_33DA:
        ld      a,(numtry)
        cp      5
        jp      m,L_33EB
        ld      de,L_5065
        call    error3
        jp      abort
L_33EB:
        inc     a
        ld      (numtry),a
        ld      hl,remdat
        ld      (datptr),hl
        ld      (numtry),a
        ld      hl,remdat
        ld      (datptr),hl
        ld      hl,L_5B54
        ld      (L_5A7E),hl
        ld      b,$01
L_3406:
        ld      hl,(L_5A7E)
        ld      a,(hl)
        inc     hl
        ld      (L_5A7E),hl
        ld      c,a
        ld      a,(L_5901)
        or      a
        ld      a,c
        jp      nz,L_3438
        ld      a,(parity)
        cp      $4E
        ld      a,c
        jp      z,L_3438
        ld      a,(wrn8bl)
        or      a
        jp      nz,L_3435
        ld      a,c
        and     $80
        jp      z,L_3435
        call    parwrn
        ld      a,$FF
        ld      (wrn8bl),a
L_3435:
        ld      a,c
        and     $7F
L_3438:
        ld      hl,(datptr)
        ld      (hl),a
        inc     hl
        ld      (datptr),hl
        inc     b
        ld      a,(L_5A82)
        cp      b
        jp      p,L_3406
        ld      a,(L_5A82)
        ld      (L_5BC0),a
        ld      a,(pktnum)
        ld      (argblk),a
        ld      a, 'D'
        call    spack
        jp      abort
L_345C:
        call    rpack
        jp      r_____
L_3462:
        cp      'Y'
        jp      nz,L_34B8
        call    comppn
        ret     nz
        ld      a,(argblk)
        call    countp
        ld      a,(numtry)
        ld      (oldtry),a
        xor     a
        ld      (numtry),a
        ld      a,(L_5BC0)
        cp      1
        jp      nz,L_3496
        ld      a,(remdat)
        cp      'Z'
        jp      nz,L_348E
        ld      (czseen),a
L_348E:
        cp      'X'
        jp      nz,L_3496
        ld      (czseen),a
L_3496:
        ld      a,(czseen)
        or      a
        jp      z,L_34A3
        ld      a,$5A
        ld      (state),a
        ret
L_34A3:
        call    L_3777
        jp      L_34AD
L_34A9:
        ld      (L_5A82),a
        ret
L_34AD:
        cp      -1
        jp      nz,abort
        ld      a, 'Z'
        ld      (state),a
        ret
L_34B8:
        cp      'N'
        jp      nz,L_34CF
        call    updrtr
        ld      a,(pktnum)
        inc     a
        and     $3F
        ld      b,a
        ld      a,(argblk)
        cp      b
        ret     nz
        jp      L_34A3
L_34CF:
        cp      'E'
        jp      nz,abort
        call    error_
        jp      abort
L_34DA:
        ld      a,(numtry)
        cp      5
        jp      m,L_34EB
        ld      de,L_5065
        call    error3
        jp      abort
L_34EB:
        inc     a
        ld      (numtry),a
        ld      a,(pktnum)
        ld      (argblk),a
        xor     a
        ld      (L_5BC0),a
        ld      a,(czseen)
        or      a
        jp      z,L_350A
        ld      a, 'D'
        ld      (remdat),a
        ld      a, 1
        ld      (L_5BC0),a
L_350A:
        ld      a, 'Z'
        call    spack
        jp      abort
L_3512:
        call    rpack
        jp      r_____
L_3518:
        cp      'Y'
        jp      nz,L_3557
        call    comppn
        ret     nz
L_3521:
        call    countp
        ld      a,(numtry)
        ld      (oldtry),a
        xor     a
        ld      (numtry),a
        call    L_4B16
        ld      a,(czseen)
        cp      'Z'
        jp      z,L_3551
L_3539:
        call    wch_nx
        jp      c,L_3551
        cp      $11
        jp      nz,L_3539
        call    getfil
        xor     a
        ld      (czseen),a
        ld      a, 'F'
        ld      (state),a
        ret
L_3551:
        ld      a, 'B'
        ld      (state),a
        ret
L_3557:
        cp      'N'
        jp      nz,L_356E
        call    updrtr
        ld      a,(pktnum)
        inc     a
        and     $3F
        ld      b,a
        ld      a,(argblk)
        cp      b
        ret     nz
        jp      L_3521
L_356E:
        cp      'E'
        jp      nz,abort
        call    error_
        jp      abort
L_3579:
        ld      a,(numtry)
        cp      5
        jp      m,L_358A
        ld      de,L_5065
        call    error3
        jp      abort
L_358A:
        inc     a
        ld      (numtry),a
        ld      a,(pktnum)
        ld      (argblk),a
        xor     a
        ld      (L_5BC0),a
        ld      a, 'B'
        call    spack
        jp      abort
L_35A0:
        call    rpack
        jp      r_____
L_35A6:
        cp      'Y'
        jp      nz,L_35C2
        call    comppn
        ret     nz
L_35AF:
        call    countp
        ld      a,(numtry)
        ld      (oldtry),a
        xor     a
        ld      (numtry),a
        ld      a, 'C'
        ld      (state),a
        ret
L_35C2:
        cp      'N'
        jp      nz,L_35D9
        call    updrtr
        ld      a,(pktnum)
        inc     a
        and     $3F
        ld      b,a
        ld      a,(argblk)
        cp      b
        ret     nz
        jp      L_35AF
L_35D9:
        cp      'E'
        jp      nz,abort
        call    error_
        jp      abort
L_35E4:
        ld      a,(L_5C28)
        add     a,$20
        ld      (hl),a
        inc     hl
        ld      a,(L_5C2A)
        add     a,$20
        ld      (hl),a
        inc     hl
        ld      a,(L_5C2C)
        add     a,$20
        ld      (hl),a
        inc     hl
        ld      a,(L_5C2E)
        add     a,$40
        and     $7F
        ld      (hl),a
        inc     hl
        ld      a,(L_5C30)
        add     a,$20
        ld      (hl),a
        inc     hl
        ld      a,(L_5C32)
        ld      (hl),a
        inc     hl
        ld      (hl), 'Y'
        ld      a,(parity)
        cp      'N'
        jp      z,L_361C
        ld      a,(L_5900)
        ld      (hl),a
L_361C:
        inc     hl
        ld      a,(L_5C33)
        ld      (hl),a
        inc     hl
        ld      a,$08
        ret
L_3625:
        ld      (L_5BBD),a
        ld      a,$0D
        ld      (L_5C2F),a
        ld      a,$23
        ld      (L_5C31),a
        ld      a,$26
        ld      (L_5900),a
        ld      a,$31
        ld      (L_5A84),a
        ld      a,(hl)
        sbc     a,$20
        ld      (L_5C27),a
        ld      a,(L_5BBD)
        cp      $03
        ret     m
        inc     hl
        inc     hl
        ld      a,(hl)
        sbc     a,$20
        ld      (L_5C2B),a
        ld      a,(L_5BBD)
        cp      $04
        ret     m
        inc     hl
        ld      a,(hl)
        add     a,$40
        and     $7F
        ld      (L_5C2D),a
        ld      a,(L_5BBD)
        cp      $05
        ret     m
        inc     hl
        ld      a,(hl)
        sbc     a,$20
        ld      (L_5C2F),a
        ld      a,(L_5BBD)
        cp      $06
        ret     m
        inc     hl
        ld      a,(hl)
        ld      (L_5C31),a
        ld      a,(L_5BBD)
        cp      $07
        ret     m
        inc     hl
        ld      a,$00
        ld      (L_5901),a
        ld      a,(hl)
        cp      $4E
        jp      z,L_36AB
        cp      $20
        jp      z,L_36AB
        cp      $59
        jp      nz,L_36A3
        ld      a,(parity)
        cp      $4E
        jp      z,L_36AB
        ld      a,$FF
        ld      (L_5901),a
        jp      L_36AB
L_36A3:
        ld      (L_5900),a
        ld      a,$FF
        ld      (L_5901),a
L_36AB:
        ld      a,(L_5BBD)
        cp      $08
        ret     m
        inc     hl
        ld      a,(hl)
        ld      b,a
        ld      a,(L_5C33)
        cp      b
        ret     nz
        ld      (L_5A84),a
        ret
L_36BD:
        ld      (L_5BBA),a
        ld      hl,remdat
        ld      (L_5A76),hl
        ld      a,(L_5C32)
        ld      b,a
        ld      c,$00
        ld      a,(L_5901)
        or      a
        jp      z,L_36D7
        ld      a,(L_5900)
        ld      c,a
L_36D7:
        ld      hl,L_5BBA
        dec     (hl)
        jp      m,rskp__
        ld      a,(L_59E4)
        and     a
        jp      nz,L_36F2
        ld      hl,L_5A71
        dec     (hl)
        jp      p,L_36F2
        call    L_4AC6
        jp      L_3770
L_36F2:
        ld      hl,(L_5A76)
        ld      a,(hl)
        inc     hl
        ld      (L_5A76),hl
        ld      e,$00
        cp      c
        jp      nz,L_370E
        ld      e,$80
        ld      a,(L_5BBA)
        dec     a
        ld      (L_5BBA),a
        ld      a,(hl)
        inc     hl
        ld      (L_5A76),hl
L_370E:
        cp      b
        jp      nz,L_3730
        ld      a,(hl)
        inc     hl
        ld      (L_5A76),hl
        ld      hl,L_5BBA
        dec     (hl)
        ld      d,a
        and     $80
        or      e
        ld      e,a
        ld      a,d
        and     $7F
        cp      b
        jp      z,L_3730
        cp      c
        jp      z,L_3730
        ld      a,d
        add     a,$40
        and     $7F
L_3730:
        or      e
        ld      (L_5BBC),a
        ld      a,(L_59E4)
        and     a
        ld      a,(L_5BBC)
;        jp      z,L_3749
;        push    hl
;        push    de
;        push    bc
;        oz      OS_Out
;        pop     bc
;        pop     de
;        pop     hl
;        jp      L_36D7
        jr      nz, L_36D7
;L_3749:
        ld      a,(L_5C25)
        cp      $02
        ld      a,(L_5BBC)
        jp      z,L_375E
        cp      $0A
        jp      z,L_3769
        cp      $1A
        jp      z,L_3769
L_375E:
        ld      hl,(L_5A78)
        ld      (hl),a
        inc     hl
        ld      (L_5A78),hl
        jp      L_36D7
L_3769:
        ld      hl,L_5A71
        inc     (hl)
        jp      L_36D7
L_3770:
        ld      de,L_505B
        call    error3
        ret
L_3777:
        ld      a,(L_5C31)
        ld      c,a
        ld      a,(curchk)
        sub     $31
        ld      b,a
        ld      a,(L_5C27)
        sub     $05
        sub     b
        ld      (L_5BBA),a
        ld      hl,L_5B54
        ld      (L_5A7E),hl
        ld      b,$00
L_3792:
        ld      a,(L_5BBA)
        dec     a
        jp      p,L_379D
        ld      a,b
        jp      rskp__
L_379D:
        ld      (L_5BBA),a
        ld      a,(L_5A71)
        dec     a
        jp      m,L_37AD
        ld      (L_5A71),a
        jp      L_37BE
L_37AD:
        call    inbuf
        jp      L_37B6
L_37B3:
        jp      L_37BE
L_37B6:
        ld      a,b
        or      a
        jp      nz,rskp__
        jp      L_3870
L_37BE:
        ld      hl,(L_5A78)
        ld      a,(L_5C25)
        cp      $02
        jp      z,L_37DE
        ld      a,(hl)
        cp      $0D
        jp      nz,L_37DF
        ld      a,$0A
        ld      (hl),a
        ld      a,(L_5A71)
        inc     a
        ld      (L_5A71),a
        ld      a,$0D
        jp      L_37E3
L_37DE:
        ld      a,(hl)
L_37DF:
        inc     hl
        ld      (L_5A78),hl
L_37E3:
        ld      d,a
        and     $80
        ld      e,a
        jp      z,L_380B
        ld      a,(L_5901)
        or      a
        jp      z,L_380B
        ld      a,(L_5BBA)
        cp      $02
        jp      m,L_3861
        dec     a
        ld      (L_5BBA),a
        ld      hl,(L_5A7E)
        ld      a,(L_5900)
        ld      (hl),a
        inc     hl
        ld      (L_5A7E),hl
        inc     b
        ld      e,$00
L_380B:
        ld      a,d
        and     $7F
        ld      d,a
        cp      $20
        jp      m,L_383D
        cp      $7F
        jp      z,L_383D
        ld      a,(L_5901)
        or      a
        jp      z,L_3827
        ld      a,(L_5900)
        cp      d
        jp      z,L_382C
L_3827:
        ld      a,d
        cp      c
        jp      nz,L_3854
L_382C:
        ld      hl,L_5BBA
        dec     (hl)
        ld      hl,(L_5A7E)
        ld      (hl),c
        inc     hl
        ld      (L_5A7E),hl
        inc     b
        ld      a,d
        jp      L_3854
L_383D:
        ld      (temp2),a
        ld      hl,L_5BBA
        dec     (hl)
        ld      hl,(L_5A7E)
        ld      (hl),c
        inc     hl
        ld      (L_5A7E),hl
        inc     b
        ld      a,(temp2)
        add     a,$40
        and     $7F
L_3854:
        ld      hl,(L_5A7E)
        or      e
        ld      (hl),a
        inc     hl
        ld      (L_5A7E),hl
        inc     b
        jp      L_3792
L_3861:
        ld      hl,(L_5A78)
        dec     hl
        ld      (L_5A78),hl
        ld      hl,L_5A71
        inc     (hl)
        ld      a,b
        jp      rskp__
L_3870:
        ld      a,$FF
        ret

; -----------------------------------------------------------------------------
;
;       CPSPK2.ASM
;
;       This file contains the (system-independent) routines that implement
;       the KERMIT protocol, and the commands that use them:
;       RECEIVE, SEND, FINISH, and LOGOUT.
;
pk2ver:
        defm    "Z88PK2 (04) 04-Nov-90", $00

L_3889: ; gofile ??
        call    L_4C9E
        ld      de,remdat
        call    prtstr
        ld      hl,remdat
        ld      a,(L_5C1B)
        or      a
        jp      z,L_38A2
        ld      hl,remnam
        jp      L_38FD
L_38A2:
        ld      de,buffer128
        ld      c,$FF
        ld      b,c
L_38A8:
        ld      a,(hl)
        or      a
        jp      z,L_38C8
        oz      Gn_cls
        jp      c,L_38C8
        jp      z,L_38C8
        cp      $2E
        jp      z,L_38C0
L_38BB:
        ld      a,$5A
        jp      L_38C8
L_38C0:
        ld      a,b
        cp      $FF
        jp      nz,L_38BB
        ld      a,(hl)
        ld      b,c
L_38C8:
        ld      (de),a
        inc     c
        inc     de
        inc     hl
        or      a
        jp      nz,L_38A8
        ex      de,hl
        dec     hl
        dec     hl
        ld      a,(hl)
        cp      $2E
        jp      nz,L_38DE
        xor     a
        ld      (hl),a
        jp      L_38FA
L_38DE:
        ld      a,b
        cp      $FF
        jp      z,L_38FA
        ld      c,b
        ld      b,$00
        inc     c
        ld      hl,buffer128
        add     hl,bc
        ld      c,$03
L_38EE:
        inc     hl
        ld      a,(hl)
        or      a
        jp      z,L_38FA
        dec     c
        jp      p,L_38EE
        xor     a
        ld      (hl),a
L_38FA:
        ld      hl,buffer128
L_38FD:
        ld      bc,$0050
        ld      de,flnbuf
        oz      Gn_fex
        jp      c,errorbox
        ex      de,hl
        dec     hl
        ld      (L_5A7A),hl
        ld      a,$31
        ld      (L_5D32),a
        call    L_4CA7
        ld      de,flnbuf
        call    prtstr
        ld      a,(L_5C24)
        or      a
        jp      z,L_3974
        call    L_3984
        jp      nc,L_3931
        cp      $12
        jp      nz,errorbox
        jp      L_3974
L_3931:
        ld      de,L_51B4
        call    error3
        oz      Gn_cl
        jp      c,errorbox
L_393D:
        ld      hl,(L_5A7A)
        ld      a,(L_5D32)
        cp      $3A
        jp      z,L_3964
        ld      (hl),a
        inc     a
        ld      (L_5D32),a
        call    L_3984
        jp      nc,L_396E
        cp      $12
        jp      nz,errorbox
        call    L_4CA7
        ld      de,flnbuf
        call    prtstr
        jp      L_3974
L_3964:
        call    screrr
        oz      OS_Pout
        defm    "Unable to rename file",0
        ret
L_396E:
        oz      Gn_cl
        jp      L_393D
L_3974:
        call    L_4A8F
        jp      L_397D
L_397A:
        jp      rskp__
L_397D:
        ld      de,L_505B
        call    error3
        ret
L_3984:
        ld      bc,$0028
        ld      de,L_5D0A
        ld      hl,flnbuf
        ld      a, OP_IN
        oz      GN_Opf
        ret

;       Packet routines
;       ---------------

;
;       Send_Packet
;       This routine assembles a packet from the arguments given and sends it
;       to the host.
;
;       Expects the following:
;               A        - Type of packet (D,Y,N,S,R,E,F,Z,T)
;               ARGBLK   - Packet sequence number
;               ARGBLK+1 - Number of data characters
;       Returns: nonskip if failure
;                skip if success
;       called by: read, rinit, rfile, rdata, sinit, sfile, sdata, seof, seot,
;                  finish, logout, nak, ackp
;
spack:
        ld      (L_5BC1),a
        ld      hl,L_5A8F
        ld      a,(L_58FB)
        ld      (hl),a
        inc     hl
        ld      a,(curchk)
        sub     $31
        ld      b,a
        ld      a,(L_5BC0)
        add     a,$23
        add     a,b
        ld      (hl),a
        inc     hl
        ld      bc,$0000
        ld      c,a
        ld      a,(argblk)
        add     a,$20
        ld      (hl),a
        inc     hl
        add     a,c
        ld      c,a
        ld      a,$00
        adc     a,b
        ld      b,a
        ld      a,(L_5BC1)
        ld      (hl),a
        inc     hl
        add     a,c
        ld      c,a
        ld      a,$00
        adc     a,b
        ld      b,a
L_39C8:
        ld      a,(L_5BC0)
        or      a
        jp      z,L_39DE
        dec     a
        ld      (L_5BC0),a
        ld      a,(hl)
        inc     hl
        add     a,c
        ld      c,a
        ld      a,$00
        adc     a,b
        ld      b,a
        jp      L_39C8
L_39DE:
        ld      a,(curchk)
        cp      $32
        jp      z,L_3A0F
        jp      nc,L_39F8
        ld      a,c
        and     $C0
        rlca
        rlca
        add     a,c
        and     $3F
        add     a,$20
        ld      (hl),a
        inc     hl
        jp      L_3A26
L_39F8:
        ld      (hl),$00
        push    hl
        ld      hl,L_5A90
        call    crcclc
        pop     hl
        ld      c,e
        ld      b,d
        ld      a,d
        rlca
        rlca
        rlca
        rlca
        and     $0F
        add     a,$20
        ld      (hl),a
        inc     hl
L_3A0F:
        ld      a,b
        and     $0F
        rlca
        rlca
        ld      b,a
        ld      a,c
        rlca
        rlca
        and     $03
        or      b
        add     a,$20
        ld      (hl),a
        inc     hl
        ld      a,c
        and     $3F
        add     a,$20
        ld      (hl),a
        inc     hl
L_3A26:
        ld      a,(dbgflg)
        or      a
        jp      z,L_3A3A
        push    hl
        ld      (hl),$00
        call    L_4D54
        ld      hl,L_5A90
        oz      GN_Sop
        pop     hl
L_3A3A:
        ld      a,(L_5C2F)
        ld      (hl),a
        inc     hl
        xor     a
        ld      (hl),a
        ld      a,(L_5C2B)
        ld      (L_5BBA),a
L_3A47:
        ld      a,(L_5BBA)
        dec     a
        or      a
        jp      m,L_3A5C
        ld      (L_5BBA),a
        ld      a,(L_5C2D)
        ld      e,a
        call    si_putbyte
        jp      L_3A47
L_3A5C:
        ld      hl,L_5A8F
L_3A5F:
        ld      a,(hl)
        or      a
        jp      z,L_3A77
        ld      e,a
        call    si_putbyte
        ld      a,(L_5C34)
        cp      (hl)
        jp      nz,L_3A73
        ld      e,a
        call    si_putbyte
L_3A73:
        inc     hl
        jp      L_3A5F
L_3A77:
        jp      rskp__

;
;       Receive_Packet
;       This routine waits for a packet to arrive from the host.  It reads
;       characters until it finds a SOH.  It then reads the packet into packet.
;
;       Returns:  nonskip if failure (checksum wrong or packet trashed)
;          skip if success, with
;               A        - message type
;               ARGBLK   - message number
;               ARGBLK+1 - length of data
;       called by: rinit, rfile, rdata,
;                  sinit, sfile, sdata, seof, seot, finish, logout
;
rpack:
        call    inpkt                           ; inpkt
        jp      r_____                          ; r
rpack0:
        call    getchr                          ; getchr
        jp      rpack                           ; rpack
L_3A86:
        ld      hl,L_58FA
        cp      (hl)
        jp      nz,rpack0
rpack1:
        call    getchr
        jp      r_____
L_3A93:
        ld      hl,L_58FA                       ; rcvsop
        cp      (hl)
        jp      z,rpack1
        ld      (L_5A90),a                      ; packet+1
        ld      c,a
        ld      a,(curchk)                      ; curchk
        sub     $31
        ld      b,a
        ld      a,c
        sub     $23
        sub     b
        ld      (L_5BC0),a                      ; argblk+1
        ld      b,$00
        call    getchr
        jp      r_____
L_3AB3:
        ld      hl,L_58FA
        cp      (hl)
        jp      z,rpack1
        ld      (argblk),a
        ld      (L_5A91),a
        add     a,c
        ld      c,a
        ld      a,$00
        adc     a,b
        ld      b,a
        ld      a,(argblk)
        sub     $20
        ld      (argblk),a
        call    getchr
        jp      r_____
L_3AD4:
        ld      hl,L_58FA
        cp      (hl)
        jp      z,rpack1
        ld      (L_5BBA),a
        ld      (L_5A92),a
        add     a,c
        ld      c,a
        ld      a,$00
        adc     a,b
        ld      b,a
        ld      a,(L_5BC0)
        ld      (temp2),a
        ld      hl,remdat
        ld      (datptr),hl
rpack2:
        ld      a,(temp2)
        sub     $01
        jp      m,rpack3
        ld      (temp2),a
        call    getchr
        jp      r_____
L_3B04:
        ld      hl,L_58FA
        cp      (hl)
        jp      z,rpack1
        ld      hl,(datptr)
        ld      (hl),a
        inc     hl
        ld      (datptr),hl
        add     a,c
        ld      c,a
        ld      a,$00
        adc     a,b
        ld      b,a
        jp      rpack2
rpack3:
        call    getchr
        jp      r_____
L_3B22:
        ld      hl,L_58FA
        cp      (hl)
        jp      z,rpack1
        sub     $20
        ld      (L_5BBC),a
        ld      a,(curchk)
        cp      $32
        jp      z,L_3B7B
        jp      nc,L_3B4D
        ld      a,c
        and     $C0
        rlca
        rlca
        add     a,c
        and     $3F
        ld      b,a
        ld      a,(L_5BBC)
        cp      b
        jp      z,L_3BA6
L_3B49:
        call    updrtr
        ret

;Here for three character CRC-CCITT

L_3B4D: ; rpack5
        ld      hl,(datptr)
        ld      (hl),$00
        ld      hl,L_5A90                       ; packet+1
        call    crcclc
        ld      c,e
        ld      b,d
        ld      a,d
        rlca
        rlca
        rlca
        rlca
        and     $0F
        ld      d,a
        ld      a,(L_5BBC)
        cp      d
        jp      nz,L_3B49
        call    getchr
        jp      r_____
L_3B6F:
        ld      hl,L_58FA
        cp      (hl)
        jp      z,rpack1
        sub     $20
        ld      (L_5BBC),a

;Here for a two character checksum and last two characters of CRC

L_3B7B: ; rpack4
        ld      a,b
        and     $0F
        rlca
        rlca
        ld      b,a
        ld      a,c
        rlca
        rlca
        and     $03
        or      b
        ld      b,a
        ld      a,(L_5BBC)
        cp      b
        jp      nz,L_3B49
        call    getchr
        jp      r_____
L_3B95:
        ld      hl,L_58FA
        cp      (hl)
        jp      z,rpack1
        sub     $20
        ld      b,a
        ld      a,c
        and     $3F
        cp      b
        jp      nz,L_3B49
L_3BA6:
        ld      hl,(datptr)
        ld      (hl),$00
        ld      a,(L_5BBA)
        jp      rskp__

;
;       inpkt - receive and buffer packet
;       returns: nonskip if error (timeout)
;               skip if success; packet starts at recpkt (which holds the SOH)
;               and is terminated by a null.
;               console is selected in either case.
;       called by: rpack
;
inpkt: ; inpkt
        ld      hl,recpkt  
        ld      (pktptr),hl
L_3BB7:
        call    inchr
        jp      r_____
L_3BBD:
        ld      hl,L_58FA                       ; rcvsop (start of packet)
        cp      (hl)
        jp      nz,L_3BB7
        jp      L_3BDA
L_3BC7:
        call    inchr
        jp      r_____
L_3BCD:
        ld      hl,L_58FA
        cp      (hl)
        jp      nz,L_3BDA
        ld      hl,recpkt
        ld      (pktptr),hl
L_3BDA:
        ld      hl,(pktptr)
        ld      (hl),a
        inc     hl
        ld      (pktptr),hl
        ld      b,a
        ld      de,$A4AE                        ; -recpkx
        add     hl,de
        jp      c,inpkt
        ld      a,(L_5C30)                      ; reol
        cp      b
        jp      nz,L_3BC7
        ld      hl,L_5AF0                       ; repkt+3
        ld      a,(L_5A92)                      ; packet+3
        cp      (hl)
        jp      z,inpkt
        ld      hl,(pktptr)
        dec     hl
        ld      (hl),$00
        ld      a,(dbgflg)
        or      a
        jp      z,L_3C12
        inc     hl
        call    L_4D4B
        ld      hl,L_5AEE
        oz      GN_Sop
L_3C12:
        ld      hl,recpkt
        ld      (pktptr),hl
        jp      rskp__

;
;       getchr - get next character from buffered packet.
;       returns nonskip at end of packet.
;       called by: rpack
;
getchr: ; getchr
        ld      hl,(pktptr)
        ld      a,(hl)
        inc     hl
        ld      (pktptr),hl
        or      a
        jp      nz,rskp__                       ; rskp
        ret
;
;       inchr - character input loop for file transfer
;       returns: nonskip if timeout or character typed on console
;                       (console selected)
;               skip with character from modem in A (parity stripped
;                       if necessary; modem selected)
;               preserves bc, de, hl in either case.
;       called by: inpkt
;
inchr:
        push    hl
        push    bc
        ld      hl,(timout)
        ld      (timval),hl
L_3C30:
        call    si_getbyte                      ; inpmdm
        or      a
        jp      z,L_3C48
        ld      b,a
        ld      a,(parity) 
        cp      'N'                             ; none
        ld      a,b
        jp      z,L_3C43
        and     $7F                             ; turn off parity bit
L_3C43:
        pop     bc
        pop     hl
        jp      rskp__                          ; rskp (take skiip return)
L_3C48:
        call    inpcon
        or      a
        jp      z,L_3C6B
        cp      $0D                             ; CR
        jp      z,L_3C68
        cp      $1A                             ; <>Z
        jp      z,L_3C63
        cp      $03                             ; <>C
        jp      z,L_3C84
        cp      $18                             ; <>X
        jp      nz,L_3C6B
L_3C63:
        add     a,$40
        ld      (czseen),a
; !! bug ??
; jr L_3C6B
L_3C68:
        pop     bc
        pop     hl
        ret
L_3C6B:
        ld      a,(timflg)
        or      a
        jp      z,L_3C30
        ld      hl,(timval)
        dec     hl
        ld      (timval),hl
        ld      a,h
        or      l
        jp      nz,L_3C30
        call    updrtr
        pop     bc
        pop     hl
        ret
L_3C84:
        call    ClearScreen
        jp      kermit

;
;       CRCCLC - Routine to calculate a CRC-CCITT for a string.
;
;       This routine will calculate a CRC using the CCITT polynomial for
;       a string.
;
;       call with: HL/ Address of null-terminated string
;       16-bit CRC value is returned in DE.
;       Registers BC and HL are preserved.
;
;       called by: spack, rpack
;
crcclc:
        push    hl
        push    bc
        ld      de,$0000
L_3C8F:
        ld      a,(hl)
        or      a
        jp      z,L_3CBD
        push    hl
        xor     e
        ld      e,a
        and     $0F
        ld      c,a
        ld      b,$00
        ld      hl,crctb2
        add     hl,bc
        add     hl,bc
        push    hl
        ld      a,e
        rrca
        rrca
        rrca
        and     $1E
        ld      c,a
        ld      hl,crctab
        add     hl,bc
        ld      a,(hl)
        xor     d
        inc     hl
        ld      d,(hl)
        pop     hl
        xor     (hl)
        ld      e,a
        inc     hl
        ld      a,(hl)
        xor     d
        ld      d,a
        pop     hl
        inc     hl
        jp      L_3C8F
L_3CBD:
        pop     bc
        pop     hl
        ret

; 3CC0h as defw...
crctab:
        defb    $00,$00,$81,$10,$02,$21,$83,$31,$04,$42,$85,$52,$06,$63,$87,$73	; .....!.1.B.R.c.s
        defb    $08,$84,$89,$94,$0a,$a5,$8b,$b5,$0c,$c6,$8d,$d6,$0e,$e7,$8f,$f7	; ................
crctb2:
        defb    $00,$00,$89,$11,$12,$23,$9b,$32,$24,$46,$ad,$57,$36,$65,$bf,$74	; .....#.2$F.W6e.t
        defb    $48,$8c,$c1,$9d,$5a,$af,$d3,$be,$6c,$ca,$e5,$db,$7e,$e9,$f7,$f8	; H...Z...l...~...

error_: ; error
        call    screrr
        ld      a, 'A'
        ld      (state),a                      ; state
        jp      error2
error1: ; error1
        oz      Gn_nln
        jp      c,errorbox
error2: ; error2
        ld      a,(L_5BC0)                      ; argblk+1
        ld      c,a
        ld      b,$00
        ld      hl,remdat
        add     hl,bc
        ld      a,$00
        ld      (hl),a
        ld      de,remdat                       ; data
        call    prtstr
        ret
error3: ; error3
        push    de
        call    screrr
        pop     de
        call    prtstr
        ret


ClsAppNameVersion:
        oz      OS_Pout
        defm    FF,"Kermit-88 v1.04",0
        ret

;
;       Set up for file transfer.
;       called by read, send.
;
trnscr: ; init
        call    ClsAppNameVersion
        oz      OS_Pout
        defm    CR,LF,LF, "Packets:                Remote file:"
        defm    CR,LF, "Retries:                Local file :",0
        call    L_4CB0
        oz      OS_Pout
        defm    1,"3+RFWaiting",1,"4-RFC",0
        ld      a,(dbgflg)
        or      a
        ret     z
        oz      OS_Pout
        defm    CR,LF,"Spack:"
        defm    CR,LF,"Rpack:",0
        call    si_flushrxtx
        ret

;
;       Set state to ABORT
;       called by: rinit, rfile, rdata, sinit, sfile, sdata, seof, seot,
;                  nak, ackp
;
abort:
        ld      a, 'A'
        ld      (state),a      ; state
        ret

;
;       nak - send NAK packet
;       here from: rinit, rfile, rdata
;       nak0 - update retry count and send NAK packet
;       here from: rinit, rfile, rdata, tryagn
;
sdnak0: ; nak0
        call    updrtr
sdnak_: ; nak
        ld      a,(pktnum)      ; pktnum
        ld      (argblk),a      ; argblk
        xor     a
        ld      (L_5BC0),a      ; argblk+1
        ld      a, 'N'
        call    spack          ; spack
        jp      abort          ; abort

;
;       increment and display retry count
;       called by: rfile, sinit, sfile, sdata, seof, seot,
;                  nak, rpack, inchr, tryagn
;
updrtr:
        call    L_4C98
        ld      hl,(numrtr)     ; numrtr
        inc     hl
        ld      (numrtr),hl
        call    prtnum          ; nout?
        ret

;
;       [jd] this routine prints parity warnings.  All registers are
;       saved except for a.
;       called by: sdata
;
parwrn:
        push    bc
        push    de
        push    hl
        ld      de,inms25
        call    error3
        pop     hl
        pop     de
        pop     bc
        ret

;
;       print message in status field.  address of message is in DE.
;       called by: read, send
;
finmes:
        push    de
        call    L_4CB0
        pop     de
        call    prtstr
        call    L_4D5D
        ret

;
;       Compare expected packet number against received packet number.
;       return with flags set (Z = packet number valid)
;       called by: rfile, rdata, sinit, sfile, sdata, seof, seot
;
comppn:
        ld      a,(pktnum)
        ld      b,a
        ld      a,(argblk)
        cp      b
        ret

;
;       Increment the packet number, modulo 64.
;       called by: rinit, rfile, rdata, sinit, sfile, sdata, seof, seot
;
countp:
        inc     a
        and     $3F
        ld      (pktnum),a 
        ld      hl,(numpkt)
        inc     hl
        ld      (numpkt),hl
        ret

;
;       Send an ACK-packet
;       called by: rfile, rdata, tryagn
;
L_3D92: ; ackp
        xor     a
        ld      (numtry),a
        ld      (L_5BC0),a
        ld      a,$59
        call    spack
        jp      abort

; 3DA1h
        defb    $c9	; .<2.Z:.Z=.?G:.[.

;
;       ?
;       called with A/ current retry count
;       called by: rfile, rdata
;
tryagn:
        inc     a
        ld      (oldtry),a
        ld      a,(pktnum)
        dec     a
        and     $3F
        ld      b,a
        ld      a,(argblk)
        cp      b
        jp      nz,sdnak0
        call    updrtr    
        call    L_3D92
        ret

;
;       test if character in A is the start of header character.  We get
;       the start of packet character from sohchr, which can be SET
;
tstsoh:
        push    bc
        ld      c,a
        ld      a,(L_5A8E)      ; sohchr
        cp      c
        ld      a,c
        pop     bc
        ret

; -----------------------------------------------------------------------------
;
;       CPSREM.ASM
;
;       This file contains the (system-independent) routines that implement
;       the REMOTE commands of the KERMIT protocol.
;
remver:
        defm    "Z88REM (00) 20-Oct-90", $00

cmd_remote:
        ld      de,L_3DE5
        ld      hl,L_3E01
        call    keycmd
        ex      de,hl
        jp      (hl)

L_3DE5:
        defb    $03
        defm    $03, "DIR", $00
        defw    unimplemented
        defm    $06, "FINISH", $00
        defw    unimplemented
        defm    $06, "LOGOUT", $00
        defw    unimplemented
L_3E01:
        defm    $0d, $0a, $01, "TDIR     FINISH     LOGOUT"
        defm    $01, "T", $00

L_3E21:                                         ; !!! UNIMPLEMENTED !!!
L_3E2A:
        call    cfmcmd
        xor     a
        ld      (numtry),a
        ld      (czseen),a
        ld      (pktnum),a
        ld      hl,$0000
        ld      (numpkt),hl
        ld      (numrtr),hl
        ld      a,$31
        ld      (curchk),a
        ld      a,$49
        ld      (state),a
        call    L_326E
        ld      a,(state)
        cp      $58
        jp      z,L_3E67
        oz      OS_Pout
        defm    CR,LF,"Failed to exchange parameters",0
        jp      kermit
L_3E5E:
        oz      OS_Pout
        defm    CR,LF,"Too many retries",0
        jp      kermit
L_3E67:
        call    ClearScreen
        inc     a
        ld      (numtry),a
        xor     a
        ld      (argblk),a
        ld      a,$01
        ld      (L_5BC0),a
        ld      hl,remdat
        ld      (hl),$44
        ld      a,$47
        call    spack
        jp      L_3E67
L_3E84:
        call    rpack
        jp      L_3E5E
L_3E8A:
        cp      $45
        jp      z,kermit
        cp      $58
        jp      nz,L_3EA8
        ld      hl,remdat
        ld      a,(L_5BC0)
L_3E9A:
        and     a
        jp      z,L_3EA8
        dec     a
        inc     hl
        push    af
        ld      a,(hl)
        oz      OS_Out
        pop     af
        jp      L_3E9A
L_3EA8:
        ld      a,$FF
        ld      (L_59E4),a
        ld      a,$44
        ld      (state),a
        call    L_3D92
        jp      L_2ED4
L_3EB8:                                         ; !!! UNIMPLEMENTED !!!
L_3EC1:
        call    cfmcmd
        xor     a
        ld      (numtry),a
        ld      a,$31
        ld      (curchk),a
L_3ECD:
        ld      a,(numtry)
        cp      $05
        jp      m,L_3EDE
L_3ED5:
        oz      OS_Pout
        defm    CR,LF,"Unable to tell host that the session is finished",0
        jp      kermit
L_3EDE:
        inc     a
        ld      (numtry),a
        xor     a
        ld      (argblk),a
        ld      a,$01
        ld      (L_5BC0),a
        ld      hl,remdat
        ld      (hl),$46
        ld      a,$47
        call    spack
        jp      L_3ED5
L_3EF8:
        call    rpack
        jp      L_3ECD
L_3EFE:
        cp      $59
        jp      z,kermit
        cp      $45
        jp      nz,L_3ECD
        call    error1
        jp      kermit
L_3F0E:                                         ; !!! UNIMPLEMENTED !!!
;        ld      de,L_5245
;        call    L_49CF
;        jp      L_236E

L_3F17:
        call    cfmcmd
        call    L_3F23
        jp      kermit
L_3F20:
        jp      kermit
L_3F23:
        xor     a
        ld      (numtry),a
        ld      a,$31
        ld      (curchk),a
L_3F2C:
        ld      a,(numtry)
        cp      $05
        jp      m,L_3F3B
L_3F34:
        oz      OS_Pout
        defm    CR,LF,"Unable to tell host to logout",0
        ret
L_3F3B:
        inc     a
        ld      (numtry),a
        xor     a
        ld      (argblk),a
        ld      a,$01
        ld      (L_5BC0),a
        ld      hl,remdat
        ld      (hl),$4C
        ld      a,$47
        call    spack
        jp      L_3F34
L_3F55:
        call    rpack
        jp      L_3F2C
L_3F5B:
        cp      $59
        jp      z,rskp__
        cp      $45
        jp      nz,L_3F2C
        call    error1
        ret

; -----------------------------------------------------------------------------
;
;       CPSTT.ASM
;
;       This file contains the code for the TRANSMIT and CONNECT commands,
;       which communicate with a host which is not running KERMIT.
;
ttver:
        defm    "Z88TT  (00) 20-Oct-90", $00

;
;       This is the TRANSMIT command.  It attempts to send a file, even
;       though there is no KERMIT on the other side.
;       here from: kermit
;
; [OBS] I have replaced the routine, so that TRANSMIT <filename> <wait string>
; will send a line at a time to the host in a manner similar to MSKERMIT
;
cmd_transmit:
        ld      a,$05                           ; txt
        ld      de,flnbuf
        call    commnd
        jp      kermit
L_3F8A:
        ex      de,hl
        ld      (hl), 0
        ld      de,stbuff
        ld      a,$05                           ; txt
        call    commnd
        jp      kermit
L_3F98:
        ld      (strcnt),a
        and     a
        jp      nz,L_3FA9
        ld      a, 1
        ld      (strcnt),a
        ld      a, CR
        ld      (stbuff),a
L_3FA9:
        call    cfmcmd
        call    getfil                          ; getfil ??
        oz      OS_Pout
        defm    CR,LF,"[Transmitting file to host:"
        defm    CR,LF," 1. Lines automatically sent, and wait for possible reply"
        defm    CR,LF," 2. ", $01, "+C forces Connect, "
        defm    1,"+Z aborts", $0d, $0a, " 3. If transfer hangs, try "
        defm    1,$E1," to continue", $0d, $0a, " 4. on exit, you will be placed in CONNECT state"
        defm    CR,LF,0
        xor     a
        ld      (L_5961),a
        call    inbuf
        jp      L_40A9
L_3FBF:
        xor     a
        ld      (L_5962),a
        ld      (L_5963),a
L_3FC6:
        call    L_406F
        jp      L_40A9
L_3FCC:
        and     $7F
        cp      $0D
        jp      nz,L_3FC6
        xor     a
        ld      (L_5963),a
        ld      a,$2A
        oz      OS_Out
L_3FDB:
        xor     a
        ld      (L_5961),a
L_3FDF:
        call    si_getbyte
        and     $7F
        jp      nz,L_4014
        call    inpcon
        and     $7F
        jp      nz,L_3FF7
        ld      a,(strcnt)
        and     a
        jp      z,L_3FC6
        jp      L_4014
L_3FF7:
        cp      $03
        jp      z,L_40A9
        cp      $1A
        jp      nz,L_4007
        call    L_4B16
        jp      kermit
L_4007:
        cp      $0D
        jp      nz,L_3FDF
        ld      a,$01
        ld      (L_5962),a
        jp      L_3FC6
L_4014:
        ld      e,a
        ld      a,(L_5961)
        ld      hl,stbuff
        add     a,l
        ld      l,a
        ld      a,$00
        adc     a,h
        ld      h,a
        ld      a,e
        cp      (hl)
        jp      nz,L_3FDB
        ld      a,(L_5961)
        inc     a
        ld      (L_5961),a
        ld      e,a
        ld      a,(strcnt)
        sub     e
        jp      z,L_3FBF
        jp      L_3FDF
L_403E:
        cp      $20
        jp      p,L_4050
        cp      $0D
        jp      z,L_4050
        cp      $09
        jp      z,L_4050
        jp      L_406F
L_4050:
        push    af
        ld      e,a
        call    si_putbyte
        pop     af
        ld      e,a
        ld      a,(L_5C34)
        and     a
        ld      a,e
        jp      z,rskp__
        ld      a,(L_5C35)
        cp      e
        jp      nz,rskp__
        push    af
        call    si_putbyte
        pop     af
        jp      rskp__
L_406F:
        ld      a,(L_5962)
        and     a
        jp      z,L_4A6A
        ld      hl,L_5964
        ld      a,(L_5963)
        ld      d,$00
        ld      e,a
        add     hl,de
        inc     a
        ld      (L_5963),a
        ld      a,(hl)
        jp      rskp__
L_408E:
        ld      c,a
        ld      a,(L_5963)
        ld      e,a
        ld      d,$00
        ld      hl,L_5964
        add     hl,de
        inc     a
        ld      (L_5963),a
        ld      a,c
        ld      (hl),c
        jp      rskp__
L_40A9:
        call    L_4B16
        ld      a,(L_58F9)
        ld      de,L_535A
        and     a
        jp      nz,L_40B9
        ld      de,L_5440
L_40B9:
        jp      telnt1

;
;   telnet - the CONNECT command.
;       here from: kermit
;   telnt1 - entry to connect mode from TRANSMIT command
;       here from: xend
;
cmd_connect:
        call    cfmcmd
        ld      de,infms7
telnt1:
        call    prtstr
        call    escprt
        oz      OS_Pout
        defm    "C to return;", $0d, $0a, " type ",0
        call    escprt
        oz      OS_Pout
        defm    "? for command list]",CR,LF,0
        ld      a,(logflg)
        or      a
        call    nz,logopn
        call    L_4DB2
        call    prtchr
        call    conchr
        jp      kermit
L_535A:
        defm    CR,LF,"[Transmission done. Connected normally to remote host,"
        defm    CR,LF," type ",0
L_5440:
        defm    CR,LF,"[Transmission Aborted. Connected normally to remote host,"
        defm    CR,LF," type ",0
infms7:
        defm    CR,LF,"[Connecting to remote host,"
        defm    CR,LF," type ",0

;
;       prtchr - copy characters from comm line to console
;       returns: nonskip, console selected.
;       called by: xnext, rexmit, telnet
;
prtchr:
        call    si_getbyte
        or      a
        ret     z
        and     $7F
        ld      (L_5854),a
        and     a
        jp      z,prtchr
        cp      $7F
        jp      z,prtchr
        ld      e,a
        ld      a,(ws_terminal)
        cp      $01
        jp      nz,L_4124
        ld      a,(L_5A6B)
        or      a
        jp      z,L_4116
        call    vt52__
        jp      prtchr
L_4116:
        ld      a,e
        cp      $1B
        jp      nz,L_4124
        ld      a,$01
        ld      (L_5A6B),a
        jp      prtchr
L_4124:
        ld      a,(ws_terminal)
        cp      $02
        jp      nz,L_4141
        ld      a,(L_5854)
        cp      $0D
        jp      z,L_4141
        cp      $0A
        jp      z,L_4141
        cp      $09
        jp      z,L_4141
        cp      $20
        ret     m
L_4141:
        call    L_4DCF
        or      a
        jp      z,prtchr
        ld      a,(logflg)
        cp      $81
        call    z,L_41BB
        ld      a,e
        oz      OS_Out
        jp      prtchr
logopn:
        ld      hl,logfln
        ld      de,flnbuf
        ld      bc,$0050
        ldir
        ld      a,$03
L_4163:
        ld      (L_5BBA),a
        ld      hl,flnbuf
        ld      de,logfln
        ld      bc,$0050
        oz      GN_Opf
        jp      nc,L_4189
        cp      $12
        jp      nz,L_41DA
        ld      e,a
        ld      a,(L_5BBA)
        cp      $02
        ld      a,e
        jp      z,L_41DA
        ld      a,$02
        jp      L_4163
L_4189:
        ld      (L_5D39),ix
        ld      a, FA_EXT
        ld      de,$0000
        oz      OS_Frm
        ld      hl,$0000
        add     hl,bc
        ld      (L_5BBA),hl
        ex      de,hl
        ld      (L_5BBC),hl
        ld      a, FA_PTR
        ld      hl,L_5BBA
        oz      OS_Fwm
        ld      hl,logflg
        ld      a,$80
        or      (hl)
        ld      (hl),a
        ld      de,L_542A
        cp      $81
        jp      z,prtstr
        ld      de,L_5412
        jp      prtstr
L_41BB:
        ld      ix,(L_5D39)
        ld      a,e
        oz      OS_Pb
        ret     nc
        jp      L_41DA
L_41C6:
        ld      de,L_51C2
        call    prtstr
        ld      hl,logflg
        ld      a,(hl)
        and     $7F
        ld      (hl),a
        call    L_4B34
        jp      c,errorbox
        ret
L_41DA:
        oz      OS_Pout
        defm    CR,LF,"Error writing to log file",CR,LF,0
        call    L_4B34
        xor     a
        ld      (logflg),a
        ret

;
;       VT52 emulation.
;       called by: prtchr
;       A/ contents of escflg (guaranteed non-zero)
;       E/ current character
;       modem is selected.
;
vt52__:
        cp      $01
        jp      nz,L_424F
;
;       E contains the character that followed the escape.
;       valid characters are:
;       A - cursor up
;       B - cursor down
;       C - cursor right
;       D - cursor left
;       F - enter graphics mode (hard to do on a non-vt52)
;       G - exit graphics mode
;       H - home
;       I - reverse linefeed
;       J - erase to end of screen
;       K - erase to end of line
;       Y - cursor positioning leadin
;       Z - identify terminal as VT52
;       [ - enter hold-screen mode (not supported)
;       \ - exit hold-screen mode (not supported)
;       > - enter alternate-keypad mode? (not supported)
;       = - exit alternate-keypad mode? (not supported)
;
;       Invalid sequences are handled as the VT52 does - the escape and
;       the following character are swallowed, never to be seen again.
;       For <esc>E, the translation table may contain just '$' (no action),
;       or may be used as clear-and-home, as in the Heath/Zenith H19.
;
        ld      a,e
        cp      $59                             ; Y
        jp      nz,L_41F9
        ld      a,$02
        ld      (L_5A6B),a
        ret
L_41F9:
        cp      $5A
        jp      z,L_423D
        cp      $41
        jp      m,L_4238
        cp      $46
        jp      m,L_422C
        cp      $4A
        jp      nz,L_4213
        call    L_4CB9
        jp      L_4238
L_4213:
        cp      $4B
        jp      nz,L_421E
        call    L_4CE6
        jp      L_4238
L_421E:
        cp      $48
        jp      nz,L_4238
        ld      bc,$0101
        call    CursorAtBC
        jp      L_4238
L_422C:
        sub     $41
        ld      hl,L_5DCF
        ld      e,a
        ld      d,$00
        add     hl,de
        ld      a,(hl)
        oz      OS_Out
L_4238:
        xor     a
        ld      (L_5A6B),a
        ret
L_423D:
        ld      e,$1B
        call    si_putbyte
        ld      e,$2F
        call    si_putbyte
        ld      e,$4B
        call    si_putbyte
        jp      L_4238
L_424F:
        cp      $02
        jp      nz,L_4260
        ld      a,e
        sub     $1F
        ld      (L_5A70),a
        ld      a,$03
        ld      (L_5A6B),a
        ret
L_4260:
        ld      a,e
        sub     $1F
        ld      c,a
        ld      a,(L_5A70)
        ld      b,a
        call    CursorAtBC
        jp      L_4238

;
;       conchr - copy character from console to comm line, processing
;       (kermit's) escape sequences.
;       Enter and exit with console selected.
;       nonskip return: transparent mode terminated.
;       skip return:    still in transparent mode.
;       called by: rexmit, telnet
;
conchr:
        call    inpcon
        or      a
        jp      z,rskp__
        cp      $D3             ; IN_SDEL ?? shift+del ??
        jp      nz,L_427C
        ld      a,$08           ; BS
L_427C:
        ld      l,a
        and     $F8
        cp      $F8
        jp      nz,L_42A2
        ld      e,$1B
        call    si_putbyte
        ld      a,l
        and     $04
        jp      nz,L_4294
        ld      e,$5B
        call    si_putbyte
L_4294:
        ld      a,l
        sub     $F8
        ld      hl,L_580F
        ld      e,a
        ld      d,$00
        add     hl,de
        ld      e,(hl)
        jp      L_42AD
L_580F:
        defm    "PQRSDCBA"
L_42A2:
        ld      a,l
        and     $7F
        ld      e,a
        ld      a,(escchr)
        cp      e
        jp      z,L_42BD
L_42AD:
        call    si_putbyte
        ld      a,(L_5C23)
        or      a
        jp      z,rskp__
        ld      a,e
        oz      OS_Out
        jp      rskp__
L_42BD:
        call    inpcon
        or      a
        jp      z,L_42BD
        ld      b,a
        cp      $03
        jp      z,L_42D1
        and     $5F
        cp      $43
        jp      nz,L_42E2
L_42D1:
        oz      OS_Pout
        defm    1,"7#1  ~(",$80
        defm    1,"2I1"
        defm    1,"3+CS"
        defm    CR,LF,"[Connection closed]",0
        ld      a,(logflg)
        or      a
        call    m,L_41C6
        ret
L_42E2:
        cp      $53
        jp      nz,L_42FC
        call    ClearScreen
        ld      de,L_55DC
        call    prtstr
        call    L_2C03
        ld      bc,$0801
        call    CursorAtBC
        jp      rskp__
L_42FC:
        ld      a,b
        cp      $3F
        jp      nz,L_432A
        ld      a,(logflg)
        or      a
        jp      p,L_430F
        ld      de,L_551B
        call    prtstr
L_430F:
        ld      de,L_5543
        call    prtstr
        call    L_4DC9                          ; !!! RET !!!
        oz      OS_Pout
        defm    CR,LF,"Typing another ",0
        call    escprt
        oz      OS_Pout
        defm    " will send it to the host"
        defm    CR,LF,"Command>",0
        jp      L_42BD
L_432A:
        ld      a,b
        cp      $30
        jp      nz,L_4338
        xor     a
        ld      e,a
        call    si_putbyte
        jp      rskp__
L_4338:
        ld      a,(escchr)
        cp      b
        jp      nz,L_4346
        ld      e,b
        call    si_putbyte
        jp      rskp__
L_4346:
        ld      a,b
        and     $5F
        ld      a,(logflg)
        or      a
        jp      p,L_4379
        ld      a,b
        and     $5F
        cp      $52
        jp      nz,L_4366
        ld      a,$81
        ld      (logflg),a
        ld      de,L_542A
        call    prtstr
        jp      rskp__
L_4366:
        cp      $51
        jp      nz,L_4379
        ld      a,$82
        ld      (logflg),a
        ld      de,L_5412
        call    prtstr
        jp      rskp__
L_4379:
        ld      a,b
        call    sysint
        jp      L_4384
L_4380:
        ld      a,$07
        oz      OS_Out
L_4384:
        jp      rskp__

; -----------------------------------------------------------------------------
;
;
;
;       DIR and TYPE commands
;
osver:
        defm    "Z88OS  (03) 04-Nov-90", $00

cmd_directory:
        ld      de,flnbuf
        ld      a,$05
        call    commnd                          ; comnd
        jp      L_43B8
L_43A8:
        ex      de,hl
        ld      (hl),$00
        ld      (L_5A71),a
        call    cfmcmd
        ld      a,(L_5A71)
        or      a
        jp      nz,L_43C1
L_43B8:
        ld      a,$2A
        ld      (flnbuf),a
        xor     a
        ld      (L_5C3A),a
L_43C1:
        ld      a,$03
        ld      (L_5BBA),a
L_43C6:
        call    wch_nx
        jp      c,L_4415
        ld      (temp2),a
        ld      a,(L_5BBA)
        inc     a
        and     $03
        ld      (L_5BBA),a
        call    z,L_4B87
        jp      c,kermit
        oz      OS_Pout
        defm    1,"2-T",1,"2X",0
        ld      a,(L_5BBA)
        ld      e,a
        ld      d,$00
        ld      hl,$0014
        oz      Gn_m16
        ld      a,l
        add     a,$20
        oz      OS_Out
        ld      de,flnbuf
        ld      hl,buffer128
        call    L_49DA
        ld      a,(temp2)
        cp      $11
        jp      z,L_440C
        oz      OS_Pout
        defm    1,"T",0
L_440C:
        ld      hl,buffer128
        oz      GN_Sop
        jp      L_43C6
L_4415:
        oz      Gn_nln
        jp      kermit
cmd_type:
; TYPE.
        ld      a,$05
        ld      de,flnbuf
        call    commnd
        jp      kermit
L_4426:
        ex      de,hl
        ld      (hl),$00
        call    cfmcmd
        call    getfil
        oz      Gn_nln
L_4432:
        call    inbuf
        jp      L_4441
L_4438:
        ld      de,buffer128
        call    L_4B4B
        jp      nc,L_4432
L_4441:
        call    L_4B16
        jp      kermit
cmd_cd:
        ld      a,$05
        ld      de,flnbuf
        call    commnd
        jp      kermit
L_4452:
        ex      de,hl
        ld      (hl),$00
        ld      (L_5A71),a
        call    cfmcmd
        ld      a,(L_5A71)
        or      a
        jp      z,L_44A9
        ld      a,$06
        ld      hl,flnbuf
        ld      bc,$004F
        ld      de,buffer128
        oz      Gn_opf
        jp      c,errorbox
        ex      de,hl
        ld      (hl),$00
        push    af
        ld      a,$05
        oz      Os_dor
        jp      c,errorbox
        pop     af
        cp      $12
        jp      z,L_448E
        cp      $81
        jp      z,L_448E
        ld      a,$18
        jp      errorbox
L_448E:
        ld      hl,L_5C8F
        ld      bc,$8C03
        oz      Os_sp
        jp      c,errorbox
        ld      hl,L_5C8F
        ld      (hl),$00
        ld      hl,buffer128
        ld      bc,$8C00
        oz      Os_sp
        jp      c,errorbox
L_44A9:
        ld      hl,L_44C8
        ld      de,flnbuf
        ld      bc,$004E
        oz      Gn_fex
        jp      c,errorbox
        xor     a
        ex      de,hl
        ld      (hl),$00
        oz      Gn_nln
        ld      hl,flnbuf
        oz      GN_Sop
        jp      kermit
L_44C8:
        defb    '.',0

; -----------------------------------------------------------------------------
;
;       CPSCMD.ASM
;
;       This file provides a user oriented way of parsing commands.
;       It is similar to that of the COMND JSYS in TOPS-20.
;
cmdver:
        defm    "Z88CMD (00) 20-Oct-90", $00

prompt:
        pop     hl
        push    hl
        ld      (cmrprs),hl
        ld      hl, 0
        add     hl,sp
        ld      (cmostp),hl
        ld      hl,cmdbuf
        ld      (cmcptr),hl
        ld      (cmdptr),hl
        xor     a
        ld      (cmaflg),a
        ld      (cmccnt),a
        ld      a,$FF
        ld      (cmsflg),a
        oz      GN_Nln
        jp      PrintPrompt

;       This address is jumped to on reparse.
;       here from:  cmcfrm, cmkeyw, cmifil, cminbf

repars:
        ld      hl,(cmostp)
        ld      sp,hl
        ld      hl,cmdbuf
        ld      (cmdptr),hl
        ld      a,$FF
        ld      (cmsflg),a
        ld      hl,(cmrprs)
        jp      (hl)

;       This address can be jumped to on a parsing error.
;       here from:  cmkeyw, cminbf

prserr:
        ld      hl,(cmostp)
        ld      sp,hl
        ld      hl,cmdbuf
        ld      (cmcptr),hl
        ld      (cmdptr),hl
        xor     a
        ld      (cmaflg),a
        ld      (cmccnt),a
        ld      a,$FF
        ld      (cmsflg),a
        oz      GN_Nln
        call    PrintPrompt
        ld      hl,(cmrprs)
        jp      (hl)

;
;       This routine parses the specified function in A.  Any additional
;       information is in DE and HL.
;       Returns +1 on success
;               +4 on failure (assumes a JMP follows the call)
;       called by:  log, setcom, read, send, xmit, dir, era, keycmd, cfmcmd
;       and CPSREM
;
commnd:
        ld      (cmstat),a      ; cmstat
        call    cminbf          ; cminbf
        cp      $04             ; cmcfm
        jp      z,cmcfrm        ; cmcfrm
        cp      $01             ; cmkey
        jp      z,cmkeyw        ; cmkeyw
        cp      $05             ; cmtxt
        jp      z,cmtext        ; cmtext
        cp      $06             ; cmnum
        jp      z,cmnumb        ; cmnumb
        oz      OS_Pout
        defm    CR,LF,"Program error:  Invalid COMND call",0
        ret

;
;       This routine parses arbitrary text up to a CR.
;       Accepts DE:     address to put text
;       Returns in A:   number of chars in text (may be 0)
;                DE:    updated pointer
;       called by: comnd
;
cmtext:
        xor     a
        ld      (slshsn),a
        ld      (slashn),a
        ld      (slashc),a
        ex      de,hl
        ld      (cmptab),hl
        ld      b,$00
L_456E:
        call    cmgtch
        or      a
        jp      p,L_45A7
        and     $7F
        cp      $1B
        jp      nz,L_4595
        ld      a,$07
        oz      OS_Out
        xor     a
        ld      (cmaflg),a
        ld      hl,(cmcptr)
        dec     hl
        ld      (cmcptr),hl
        ld      (cmdptr),hl
        ld      hl,cmccnt
        dec     (hl)
        jp      L_456E
L_4595:
        cp      $3F
        jp      z,L_45F5
        cp      $0C
        call    z,ClearScreen
        ld      a,b
        ld      hl,(cmptab)
        ex      de,hl
        jp      rskp__
L_45A7:
        cp      $5C
        jp      nz,L_45BA
        ld      a,(slshsn)
        and     a
        cpl
        jp      nz,L_45BA
        ld      (slshsn),a
        jp      L_456E
L_45BA:
        ld      e,a
        ld      a,(slshsn)
        and     a
        ld      a,e
        jp      z,L_4601
        cp      $5C
        jp      nz,L_45CE
        ld      a,(slashn)
        jp      L_4607
L_45CE:
        sub     $30
        jp      m,L_4626
        cp      $08
        jp      p,L_4626
        ld      e,a
        ld      a,(slashn)
        add     a,a
        add     a,a
        add     a,a
        add     a,e
        ld      (slashn),a
        ld      a,(slashc)
        inc     a
        ld      (slashc),a
        cp      $03
        ld      a,(slashn)
        jp      z,L_4601
        jp      L_456E
L_45F5:
        ld      hl,cmaflg
        ld      (hl),$00
        ld      hl,(cmdptr)
        inc     hl
        ld      (cmdptr),hl
L_4601:
        call    L_4612
        jp      L_456E
L_4607:
        call    L_4612
        ld      a,$5C
        ld      (slshsn),a
        jp      L_456E
L_4612:
        inc     b
        ld      hl,(cmptab)
        ld      (hl),a
        inc     hl
        ld      (cmptab),hl
        xor     a
        ld      (slashc),a
        ld      (slashn),a
        ld      (slshsn),a
        ret
L_4626:
        oz      OS_Pout
        defm    CR,LF,"Invalid \\ parameter",0
        jp      kermit

;
;       This routine gets a number from user input.
;       Called by: comnd
;
cmnumb:
        ld      hl,$0000
        ld      (number),hl
L_4635:
        call    cmgtch
        or      a
        jp      p,L_4694
        and     $7F
        cp      $1B                             ; !!!
        cp      $20
        jp      nz,L_464E
        jp      rskp__

; 4648h
        defb    $00,$00,$00,$00,$00,$00         ; !!!

L_464E:
        cp      $20
        jp      z, rskp__
        cp      $3F
        jp      z,L_4662
        cp      $0D
        jp      z,rskp__
        jp      prserr
L_465B:                                         ; !!!
        nop
        nop
        nop
        nop
        jp      rskp__

L_57CA:
        defm    " Enter a number", $00
L_57DA:
        defm    " Confirm with ", $01, $e1, " or enter more", $00
L_4662:
        ld      hl,(number)
        ld      a,l
        or      h
        ld      de,L_57DA
        jp      nz,L_4670
        ld      de,L_57CA
L_4670:
        call    prtstr
        oz      GN_Nln
        call    PrintPrompt
        ld      hl,(cmdptr)
        ld      (hl),$00
        ld      hl,(cmcptr)
        dec     hl
        ld      (cmcptr),hl
        ld      de,cmdbuf
        call    prtstr
        xor     a
        ld      (cmaflg),a
        jp      repars
L_4692:                                         ; !!!
        ld      a, $04
L_4694:
        and     $7F
        sub     $30
        jp      c,L_46B4
        cp      $0A
        jp      nc,L_46B4
        ccf
        ld      hl,(number)
        push    hl
        pop     de
        add     hl,hl
        add     hl,hl
        add     hl,de
        add     hl,hl
        ld      d,$00
        ld      e,a
        add     hl,de
        ld      (number),hl
        jp      L_4635
L_46B4:
        oz      OS_Pout
        defm    CR,LF,"Invalid user number",0
        jp      rskp__

;
;       This routine gets a confirm.
;       called by: comnd
;
cmcfrm:
        call    cmgtch
        or      a
        ret     p
        and     $7F
        cp      $1B
        jp      nz,L_46E2
        ld      a,$07
        oz      OS_Out
        xor     a
        ld      (cmaflg),a
        ld      hl,(cmcptr)
        dec     hl
        ld      (cmcptr),hl
        ld      (cmdptr),hl
        ld      hl,cmccnt
        dec     (hl)
        jp      cmcfrm
L_46E2:
        cp      $3F
        jp      nz,L_470D
        oz      OS_Pout
        defm    " Confirm with ",1,$E1,0
        oz      GN_Nln
        call    PrintPrompt
        ld      hl,(cmdptr)
        ld      a,$00
        ld      (hl),a
        ld      hl,(cmcptr)
        dec     hl
        ld      (cmcptr),hl
        ld      de,cmdbuf
        call    prtstr
        xor     a
        ld      (cmaflg),a
        jp      repars
L_470D:
        cp      $0C
        call    z,ClearScreen
        jp      rskp__

;
;       This routine parses a keyword from the table pointed
;       to in DE.  The format of the table is as follows:
;
;       addr:   db      n       ;Where n is the # of entries in the table.
;               db      m       ;M is the size of the keyword.
;               db      'string$' ;Where string is the keyword.
;               db      a,b     ;Where a & b are pieces of data
;                               ;to be returned.  (Must be two of them.)
;
;       The keywords must be in alphabetical order.
;**** Note:  The data value a is returned in registers A and E.  The
;****   data value b is returned in register D.  This allows the two data
;       bytes to be stored as:
;               dw      xxx
;       and result in a correctly formatted 16-bit value in register pair
;       DE.
;       called by: comnd
;
cmkeyw:
        ld      (L_5860),hl
        ex      de,hl
        ld      (cmptab),hl
        ld      b,(hl)
        inc     hl
        ld      (L_58EB),hl
        ld      hl,(cmdptr)
        ld      (L_58ED),hl
L_4727:
        ld      a,b
        or      a
        ret     z
        ld      hl,(L_58EB)
        ld      e,(hl)
        inc     hl
L_472F:
        dec     e
        ld      a,e
        cp      $FF
        jp      m,L_4809
        call    cmgtch
        or      a
        jp      p,L_47F7
        and     $7F
        cp      $3F
        jp      nz,L_476F
        xor     a
        ld      (cmaflg),a
        ld      hl,cmccnt
        dec     (hl)
        ld      hl,(L_5860)
        ex      de,hl
        call    prtstr
        oz      GN_Nln
        call    PrintPrompt
        ld      hl,(cmdptr)
        ld      a,$00
        ld      (hl),a
        ld      hl,(cmcptr)
        dec     hl
        ld      (cmcptr),hl
        ld      de,cmdbuf
        call    prtstr
        jp      repars
L_476F:
        cp      $1B
        jp      nz,L_47D8
        xor     a
        ld      (cmaflg),a
        push    de
        push    bc
        push    hl
        call    L_4824
        jp      L_479A
L_4781:
        ld      a,$07
        oz      OS_Out
        ld      hl,(cmcptr)
        dec     hl
        ld      (cmcptr),hl
        ld      (cmdptr),hl
        ld      hl,cmccnt
        dec     (hl)
        pop     hl
        pop     bc
        pop     de
        inc     e
        jp      L_472F
L_479A:
        ld      hl,(cmcptr)
        dec     hl
        ex      de,hl
        pop     hl
        push    hl
L_47A1:
        ld      a,(hl)
        cp      $00
        jp      z,L_47B6
        inc     hl
        ex      de,hl
        ld      (hl),a
        inc     hl
        ex      de,hl
        ld      a,(cmccnt)
        inc     a
        ld      (cmccnt),a
        jp      L_47A1
L_47B6:
        ld      a,(cmccnt)
        inc     a
        ld      (cmccnt),a
        ex      de,hl
        ld      a,$20
        ld      (hl),a
        inc     hl
        ld      (cmcptr),hl
        ld      (cmdptr),hl
        pop     hl
        push    hl
        ex      de,hl
        call    prtstr
        ld      a,$20
        oz      OS_Out
        pop     hl
        pop     bc
        pop     de
        jp      L_47EB
L_47D8:
        push    hl
        push    de
        call    L_4824
        jp      L_47E9
L_47E0:
        oz      OS_Pout
        defm    CR,LF,"Ambiguous",0
        jp      prserr
L_47E9:
        pop     de
        pop     hl
L_47EB:
        inc     e
        ld      d,$00
        add     hl,de
        inc     hl
        ld      e,(hl)
        inc     hl
        ld      d,(hl)
        ld      a,e
        jp      rskp__
L_47F7:
        cp      $61
        jp      m,L_4803
        cp      $7B
        jp      p,L_4803
        and     $5F
L_4803:
        ld      d,(hl)
        inc     hl
        cp      d
        jp      z,L_472F
L_4809:
        ld      d,$00
        ld      a,e
        or      a
        jp      p,L_4812
        ld      d,$FF
L_4812:
        add     hl,de
        ld      de,$0003
        add     hl,de
        ld      (L_58EB),hl
        dec     b
        ld      hl,(L_58ED)
        ld      (cmdptr),hl
        jp      L_4727
L_4824:
        dec     b
        ret     m
        inc     e
        ld      c,e
        ld      a,e
        or      a
        ret     z
        ld      d,$00
        add     hl,de
        ld      e,$03
        add     hl,de
        ld      b,(hl)
        inc     hl
        ex      de,hl
        ld      hl,(L_58EB)
        ld      a,(hl)
        sub     c
        ld      c,a
        cp      b
        jp      z,L_483F
        ret     p
L_483F:
        ld      hl,(L_58ED)
L_4842:
        dec     c
        jp      m,rskp__
        ex      de,hl
        ld      b,(hl)
        inc     hl
        ex      de,hl
        ld      a,(hl)
        inc     hl
        cp      $61
        jp      m,L_4858
        cp      $7B
        jp      p,L_4858
        and     $5F
L_4858:
        cp      b
        ret     nz
        jp      L_4842
cmgtch:
        push    hl
        push    bc
L_485F:
        ld      a,(cmaflg)
        or      a
        call    z,cminbf
        ld      hl,(cmdptr)
        ld      a,(hl)
        inc     hl
        ld      (cmdptr),hl
        cp      $20
        jp      z,L_4878
        cp      $09
        jp      nz,L_488B
L_4878:
        ld      a,(cmsflg)
        or      a
        jp      nz,L_485F
        ld      a,$FF
        ld      (cmsflg),a
        ld      a,$20
        pop     bc
        pop     hl
        jp      L_48B3
L_488B:
        push    af
        xor     a
        ld      (cmsflg),a
        pop     af
        pop     bc
        pop     hl
        cp      $1B
        jp      z,L_48B3
        cp      $3F
        jp      z,L_48AA
        cp      $0D
        jp      z,L_48AA
        cp      $0A
        jp      z,L_48AA
        cp      $0C
        ret     nz
L_48AA:
        push    hl
        ld      hl,(cmdptr)
        dec     hl
        ld      (cmdptr),hl
        pop     hl
L_48B3:
        or      $80
        ret
cminbf:
        push    af
        push    de
        push    hl
        ld      a,(cmaflg)
        or      a
        jp      nz,L_4964
L_48C0:
        ld      hl,cmccnt
        inc     (hl)
        call    L_4E9B
        ld      hl,(cmcptr)
        ld      (hl),a
        inc     hl
        ld      (cmcptr),hl
        cp      $15
        jp      z,L_48D9
        cp      $18
        jp      nz,L_48ED
L_48D9:
        oz      OS_Pout
        defm    1,"2X ",0
        call    L_4CE6
        call    PrintPrompt
        ld      hl,cmdbuf
        ld      (cmcptr),hl
        ld      hl,cmccnt
        ld      (hl),$00
        jp      repars
L_48ED:
        cp      $08
        jp      z,L_48FA
        cp      $7F
        jp      nz,L_491B
        call    L_4D63
L_48FA:
        ld      a,(cmccnt)
        dec     a
        dec     a
        or      a
        jp      p,L_490A
        ld      e,$07
        oz      OS_Out
        jp      L_48D9
L_490A:
        ld      (cmccnt),a
        call    L_4D6F
        ld      hl,(cmcptr)
        dec     hl
        dec     hl
        ld      (cmcptr),hl
        jp      repars
L_491B:
        cp      $3F
        jp      z,L_493F
        cp      $1B
        jp      z,L_493F
        cp      $0D
        jp      z,L_4937
L_492A:
        cp      $0A
        jp      z,L_4937
        cp      $0C
        jp      nz,L_4947
        call    ClearScreen
L_4937:
        ld      a,(cmccnt)
        cp      $01
        jp      z,prserr
L_493F:
        ld      a,$FF
        ld      (cmaflg),a
        jp      L_4964
L_4947:
        ld      a,(cmccnt)
        cp      $7D
        jp      m,L_48C0
        ld      a,$07
        oz      OS_Out
        ld      a,(cmccnt)
        dec     a
        ld      (cmccnt),a
        ld      hl,(cmcptr)
        dec     hl
        ld      (cmcptr),hl
        jp      L_48C0
L_4964:
        pop     hl
        pop     de
        pop     af
        ret
PrintPrompt:
        oz      OS_Pout
        defm    CR,1,"2-T",1,"3+BCKermit>",1,"B",0
        ret

; -----------------------------------------------------------------------------
;
;       CPSUTL.ASM
;
;       Utility routines, pure and impure data.
;
utlver:
        defm    "Z88UTL (04) 04-Nov-90", $00

escprt:
        ld      a,(escchr)
        cp      $20
        jp      p,L_4997
        oz      OS_Pout
        defm    1,"+",0
        or      $40
L_4997:
        oz      OS_Out
        ret
keycmd:
        ld      a,$01
        call    commnd
        jp      L_49A3
L_49A2:
        ret
L_49A3:
        oz      OS_Pout
        defm    CR,LF,"Unrecognized command",0
        jp      kermit
cfmcmd:                                         ; L_49AC
        ld      a,$04
        call    commnd
        jp      kermt3
        ret

;       print number in HL
;
prtnum:
        ld      c,l
        ld      b,h
        ld      hl,$0002
;        ld      de,L_5D0A
        ld      de, 0
        ld      a, $40
        push    ix
        ld      ix, 7
        oz      Gn_pdn
        pop     ix
;        xor     a
;        ld      (de),a
;        ld      hl,L_5D0A
;        oz      GN_Sop
        ret
prtstr:
        ex      de,hl
        oz      GN_Sop
        ret

;       Jumping to this location is like retskp.  It assumes the instruction
;       after the call is a jmp addr.
;       here from: many places.

rskp__:
        pop     hl
        inc     hl
        inc     hl
        inc     hl
        jp      (hl)

;       Jumping here is the same as a ret.  'jmp r' is used after routines
;       that have skip returns, where the non-skip instruction must be 3 bytes.
;       here from: many places.

r_____:
        ret

L_49DA:
        push    hl
        ex      de,hl
        ld      d,$00
        ld      a,(flnlen)
        ld      e,a
        add     hl,de
        ld      a,$2F
L_49E5:
        cp      (hl)
        jp      z,L_49EF
        dec     hl
        dec     e
        jp      nz,L_49E5
        dec     hl
L_49EF:
        inc     hl
        ex      de,hl
        ld      c,$00
        pop     hl
L_49F4:
        ld      a,(de)
        inc     de
        ld      (hl),a
        or      a
        jp      z,L_4A00
        inc     hl
        inc     c
        jp      L_49F4
L_4A00:
        ld      a,c
        ret
getfil:
        xor     a
        ld      (L_5A71),a
        ld      (L_58F9),a
        ld      bc,$0028
        ld      de,L_5D0A
        ld      hl,flnbuf
        ld      a, OP_IN
        oz      GN_Opf
        jp      c,errorbox
        ld      (hndfil),ix
        ld      a,$01
        ld      (filopn),a
        ret
inbuf:
        ld      a,(L_58F9)
        or      a
        ret     nz
        push    bc
        push    de
        push    hl
L_4A2C:
        ld      bc, 128
        ld      hl, 0                           ; file to memory
        ld      de,buffer128
        ld      ix,(hndfil)
        oz      Os_mv
        nop
        jp      nc,L_4A53
        cp      $69
        jp      z,L_4A2C
        cp      $66
        jp      z,L_4A2C
        cp      $09
        jp      nz,errorbox
        ld      a,$01
        ld      (L_58F9),a
L_4A53:
        ex      de,hl
        ld      (hl),$00
        ld      hl,buffer128
        ld      (L_5A78),hl
        ld      a,$80
        sub     c
        pop     hl
        pop     de
        pop     bc
        ret     z
        dec     a
        ld      (L_5A71),a
        jp      rskp__
L_4A6A:
        ld      de,cmdbuf
L_4A6D:
        ld      a,(L_5A71)
        or      a
        jp      p,L_4A7A
        call    inbuf
        jp      L_4A8E                          ; !!!
L_4A7A:
        dec     a
        ld      (L_5A71),a
        ld      hl,(L_5A78)
        ld      a,(hl)
        inc     hl
        ld      (L_5A78),hl
        and     $7F
        jp      z,L_4A6D
        jp      rskp__
L_4A8E:                                         ; !!!
        ret
L_4A8F:
        ld      bc,$0000
        ld      hl,flnbuf
        oz      Gn_del
        jp      nc,L_4AA3
        cp      $12
        jp      z,L_4AA3
        jp      errorbox
L_4AA3:
        ld      bc,$0028
        ld      de,L_5D0A
        ld      a, OP_OUT
        oz      GN_Opf
        ret     c
        ld      hl,buffer128
        ld      (L_5A78),hl
        ld      a,$80
        ld      (L_5A71),a
        ld      (hndfil),ix
        ld      a,$01
        ld      (filopn),a
        jp      rskp__
L_4AC6:
        ld      a,$7F
L_4AC8:
        push    bc
        ld      hl,L_5A71
        sub     (hl)
        ld      c,a
        ld      b, 0
        ld      hl,buffer128
        ld      de, 0                           ; memory to file
        ld      ix,(hndfil)
        oz      Os_mv
        jp      nc,L_4AEC
        cp      $69
        jp      z,L_4AC6
        cp      $66
        jp      z,L_4AC6
        jp      L_4AFB
L_4AEC:
        ld      hl,buffer128
        ld      (L_5A78),hl
        ld      a,$7F
        ld      (L_5A71),a
        pop     bc
        jp      rskp__
L_4AFB:
        pop     bc
        call    L_4B16
        ret
L_4B00:
        ld      a,(L_5A71)
        cp      $80
        jp      z,L_4B10
        ld      a,$80
        call    L_4AC8
        jp      r_____
L_4B10:
        call    L_4B16
        jp      rskp__
L_4B16:
        ld      ix,(hndfil)
        oz      Gn_cl
        jp      c,errorbox
        xor     a
        ld      (filopn),a
        ret
L_4B25:
        ld      a,(filopn)
        or      a
        call    nz,L_4B16
        ld      a,(wchopn)
        or      a
        call    nz,wch_cl
        ret
L_4B34:
        ld      ix,(L_5D39)
        oz      Gn_cl
        ret
L_4B3C:
        xor     a
        ld      (takopn),a
        ld      ix,(hndtak)
        oz      Gn_cl
        ret     nc
        jp      errorbox
L_4B4B:
        ld      a,(de)
        inc     de
        or      a
        ret     z
        cp      $0A
        jp      z,L_4B4B
        cp      $0D
        jp      z,L_4B65
        cp      $20
        jp      p,L_4B60
        ld      a,$7F
L_4B60:
        oz      OS_Out
        jp      L_4B4B
L_4B65:
        ld      a,(L_5BBC)
        inc     a
        and     $07
        ld      (L_5BBC),a
        jp      nz,L_4B81
        push    de
L_4B72:
        ld      a, SR_Pwt
        oz      Os_sr
        pop     de
        jp      nc,L_4B81
        cp      RC_Esc
        scf
        ret     z
        jp      L_4B72
L_4B81:
        oz      Gn_nln
        jp      L_4B4B
L_4B87:
        ld      de,L_5344
        jp      L_4B4B

; -----------------------------------------------------------------------------
;
;       CPXSYS.ASM
;
;       This file contains the system-dependent code and data for KERMIT.
;       It will be probably be broken into independent files to generate
;       overlays for the various systems, one or more overlay possible
;       from each file.  For now, we will leave it in one piece.
;
sysver:
        defm    "Z88SYS (04) 04-Nov-90", $00

wch_nx:
        ld      a,(wchopn)
        cp      $01
        jp      z,wch_1
        ld      hl,flnbuf
        ld      de,buffer128
        ld      bc,$0050
        oz      Gn_fex
        jp      c,errorbox
        xor     a
        ld      b,a
        ld      hl,buffer128
        oz      Gn_opw
        jp      c,errorbox
        ld      (hnd_wc),ix
        ld      a,$01
        ld      (wchopn),a
wch_1:
        ld      ix,(hnd_wc)
        ld      de,flnbuf
        ld      c,$50
        oz      Gn_wfn
        jp      c,wch_2
        ld      hl,flnlen
        ld      (hl),c
        ret
wch_2:
        cp      RC_EOF
        jp      nz,errorbox
        call    wch_cl
        ld      a, RC_ONF
        scf
        ret
wch_cl:
        ld      ix,(hnd_wc)
        oz      Gn_wcl
        jp      c,errorbox
        xor     a
        ld      (wchopn),a
        ret
CursorAtBC:
        oz      OS_Pout
        defm    1,"3@",0
        ld      a,c
        add     a,$1F
        oz      OS_Out
        ld      a,b
        add     a,$1F
        oz      OS_Out
        ret
L_4C92:
        ld      bc,$030A
        jp      CursorAtBC
L_4C98:
        ld      bc,$040A
        jp      CursorAtBC
L_4C9E:
        ld      bc,$0326
        call    CursorAtBC
        jp      L_4CE6
L_4CA7:
        ld      bc,$0426
        call    CursorAtBC
        jp      L_4CE6
L_4CB0:
        ld      bc,$0119
        call    CursorAtBC
        jp      L_4CE6
L_4CB9:
        call    L_4CE6
        ld      a,(L_5DD9)
        cp      $27
        ret     z
        inc     a
        ld      (L_5DD9),a
        ld      b,a
        ld      a,$48
        sub     b
        ld      (L_5DDB),a
        ld      a,(L_5DE5)
        cp      $32
        ld      a,$20
        jp      nz,L_4CD8
        inc     a
L_4CD8:
        ld      (L_5DD8),a
        ld      a,(L_5DEC)
        add     a,$20
        ld      (L_5DDA),a
        jp      L_4D2D
L_4CE6:
        ld      bc, NQ_WBOX
        xor     a
        oz      OS_Nq
        ld      (L_5DE5),a      ; id
        ld      a,c
        ld      (L_5DEC),a      ; width
        ld      a,b
        ld      (L_5DED),a      ; depth
        ld      bc, NQ_WCUR
        xor     a
        oz      OS_Nq
        ld      a,b
        add     a,$20
        ld      (L_5DD9),a
        ld      (L_5DEA),a
        ld      a,c
        add     a,$20
        ld      (L_5DE9),a
        ld      l,a
        ld      a,(L_5DE5)
        cp      $32
        ld      a,l
        jp      nz,L_4D17
        inc     a
L_4D17:
        ld      (L_5DD8),a
        ld      a,$21
        ld      (L_5DDB),a
        ld      a,(L_5DEC)
        sub     c
        add     a,$20
        ld      (L_5DDA),a
        ld      a,$80
        ld      (L_5DDC),a
L_4D2D:
        ld      a,$36
        ld      (L_5DD7),a
        ld      (L_5DE0),a
        ld      hl,L_5DD4
        oz      GN_Sop
        ld      hl,L_5DE2
        oz      GN_Sop
        ret
screrr:
        ld      bc,$0701
        call    CursorAtBC
        jp      L_4CE6
L_4D4B:
        ld      bc,$0601
        call    CursorAtBC
        jp      L_4CE6
L_4D54:
        ld      bc,$0501
        call    CursorAtBC
        jp      L_4CE6
L_4D5D:
        ld      bc,$0701
        jp      CursorAtBC
L_4D63:
        ld      a,$08
        oz      OS_Out
        call    L_4D6F
        ld      a,$08
        oz      OS_Out
        ret
L_4D6F:
        ld      a,$20
        oz      OS_Out
        ld      a,$08
        oz      OS_Out
        ret
ClearScreen:
        ld      a, FF
        oz      OS_Out
        ret
L_4DB2:
        oz      OS_Pout
        defm    CR,LF,"Press ",1,$E1," to continue",0
        call    L_4E9B
        oz      OS_Pout
        defm    1,"7#2! p(",$81
        defm    1,"2I2"
        defm    1,"3+CS",0
        ret

L_4DC9:                                         ; !!!
        ret

sysint:
        and     $5F
        jp      rskp__
L_4DCF:
        ld      a,e
        ret

L_4DD1:                                         ; !!!
        ret

L_4DD2:
        defb    $08
        defm    $04, "1200", $00
        defm    $b0, $04
        defm    $05, "19200", $00
        defm    $00,"K"
        defm    $04, "2400", $00
        defm    "`", $09
        defm    $03, "300", $00
        defm    ",", $01
        defm    $05, "38400", $00
        defm    $00, $96
        defm    $03, "600", $00
        defm    "X", $02
        defm    $02, "75", $00
        defm    "K", $00
        defm    $04, "9600", $00
        defm    $80, "%"
L_4E11:
        defm    $0d, $0a, $01, "T  75      300      600     1200"
        defm    $0d, $0a, "2400     9600    19200    38400"
        defm    $01, "T", $00

L_4E58:
        ld      hl,ws_baudrate
        call    sp_txbaud
        ld      hl,ws_baudrate
        call    sp_rxbaud
        jp      si_softreset
sp_rxbaud:
        ld      bc,$8017
        ld      a,$02
        oz      Os_sp
        ret     nc
        jp      errorbox
sp_txbaud:
        ld      bc,$8016
        ld      a,$02
        oz      Os_sp
        ret     nc
        jp      errorbox
sp_parity:
        ld      bc,$8019
        ld      a,$01
        oz      Os_sp
        ret     nc
        jp      errorbox
sp_xonxoff:
        ld      bc,$8018
        ld      a,$01
        oz      Os_sp
        ret     nc
        jp      errorbox
si_softreset:
        ld      l, SI_SFT
        oz      Os_si
        ret     nc
        jp      errorbox
L_4E9B:
        ld      a,(takopn)
        or      a
        jp      nz,L_4EB4
L_4EA2:
        oz      Os_in
        jp      nc,L_4EAE
        cp      $01
        jp      nz,L_4E9B
        ld      a,$1B
L_4EAE:
        cp      $20
        ret     m
        oz      OS_Out
        ret
L_4EB4:
        call    inpcon
        cp      $1B
        jp      z,L_4EDD
        ld      ix,(hndtak)
        oz      Os_gb
        jp      nc,L_4EAE
        cp      $09
        jp      z,L_4ED7
        cp      $08
        jp      z,errorbox
        cp      $13
        jp      z,errorbox
        jp      L_4EB4
L_4ED7:
        call    L_4B3C
        jp      L_4EA2
L_4EDD:
        oz      OS_Pout
        defm    CR,LF,"Takefile aborted",CR,LF,BEL,0
        call    L_4B3C
        jp      kermit

;
; !! do it differently !!
; enable ESC detection, then use OS_Gb directly
; and use OS_Xin instead here
;
inpcon:
        ld      bc,$0000
        oz      Os_tin
        ret     nc
        cp      RC_Time
        jp      z,L_4EFC                        ; timeout
        cp      RC_Esc
        jp      nz,inpcon                       ; loop on other errors
        ld      a, ESC
        ret
L_4EFC:
        xor     a
        ret
si_putbyte:
        push    hl
        push    bc
        ld      a,e
        ld      l, SI_PBT
        ld      bc,$FFFF                        ; use default timeout
        oz      Os_si
        jp      c,errorbox
        pop     bc
        pop     hl
        ret
si_getbyte:
        ld      l, SI_GBT
        ld      bc,$FFFF                        ; inifinite timeout
        oz      Os_si
        jp      c,errorbox
        ret
si_flushrxtx:
        ld      l, SI_FRX
        oz      Os_si
        ld      l, SI_FTX
        oz      Os_si
        ret

;       ----

errhan:
        ret     z
        cp      RC_Esc
        jr      nz,erh_1
        oz      OS_Esc
        ret
erh_1:
        cp      RC_Quit
        jr      nz,erh_2
        ld      hl,(L_58F6)
        ld      a,(L_58F8)
        oz      OS_Erh
        ld      hl,(L_58F6)
        ld      a, RC_Quit - 1
        inc     a
        scf
        jp      (hl)
erh_2:
        cp      a
        ret

;       ----

errorbox:
        oz      Gn_err
        cp      RC_Quit
        jp      z,L_2586                        ; fatal error
        jp      kermit

; -----------------------------------------------------------------------------
;
;       CPSDAT.ASM
;
;       Pure and impure data areas. Previously of CPSUTL.ASM
;
datver:
        defm    "Z88DAT (04) 04-Nov-90", $00
L_4FD7:
        defm    $0d, $0a, "Not confirmed", $00
L_4FE7:
        defm    "Unable to receive initiate"
        defm    $0d, $0a, $00
L_5004:
        defm    "Unable to receive file name"
        defm    $0d, $0a, $00
L_5022:
        defm    "Unable to receive end of file"
        defm    $0d, $0a, $00
L_5042:
        defm    "Unable to receive data"
        defm    $0d, $0a, $00
L_505B:
        defm    "No Room", $0d, $0a, $00
L_5065:
        defm    "Unable to receive an acknowledgement from the host"
        defm    $0d, $0a, $00
L_5199:
        defm    $07, $01, "RCompleted", $01, "R", $00
infms4:
        defm    $07, $01, "RFailed", $01, "R", $00
L_51B4:
        defm    "Renaming file", $00
L_51C2:
        defm    $0d, $0a, "[Closing the log file]", $00
L_525A:
        defm    $07, $01, "RInterrupted", $01, "R", $00
L_539B:
        defm    $01, "3+RFSending", $01, "3-RF", $00
L_53AD:
        defm    $01, "3+RFReceiving", $01, "3-RF", $00
inms25:
        defm    $07, "Warning: eighth bit cannot be sent", $00
L_5412:
        defm    $0d, $0a, "[Logging suspended]"
L_5344:
        defm    $0d, $0a, $00
L_542A:
        defm    $0d, $0a, "[Logging resumed]"
        defm    $0d, $0a, $00
L_54D1:
        defm    $0d, $0a, "Give the start-of-packet character: ", $00
L_551B:
        defm    $0d, $0a, "Q  Suspend logging"
        defm    $0d, $0a, "R  Resume logging", $00
L_5543:
        defm    $0d, $0a, "?  This message"
        defm    $0d, $0a, "C  Close the connection"
        defm    $0d, $0a, "0  (zero) Transmit a NULL"
        defm    $0d, $0a, "S  Status of the connection", $00
L_55DC:
        defm    $0a, $01, "TBAUD-RATE", $01, "2X8TIMER"
        defm    $01, "2Xb", $01, "USEND", $01, "U     "
        defm    $01, "UREC", $01, "U", $0d, $0a, "PARITY", $01, "2X8TERMINAL"
        defm    $01, "2XPSTART-OF-PACKET   "
        defm    $01, "T", $01, "+         ", $01, "+", $01, "T"
        defm    $0d, $0a, "FLOW-CONTROL", $01, "2X8BLOCK-CHECK"
        defm    $01, "2XPPADDING", $0d, $0a, "LOCAL-ECHO"
        defm    $01, "2X8DEBUGGING", $01, "2XPPAD-CHAR"
        defm    $0d, $0a, "FILE-MODE", $01, "2X8ESCAPE"
        defm    $0d, $0a, "WARNING", $01, "2X8LOGGING TO"
        defm    $01, "T", $00
L_56E0:
        defm    " on", $00
L_56E4:
        defm    "off", $00
L_56E8:
        defm    " ASCII", $00
L_56EF:
        defm    "binary", $00
L_5709:
        defm    " none", $00
L_570F:
        defm    " mark", $00
L_5715:
        defm    "space", $00
L_571B:
        defm    "  odd", $00
L_5721:
        defm    " even", $00
L_5727:
        defm    "UNSET", $00

; 5854h
L_5854:
        defb    $00	; ................
L_5855:
        defb    $00	; ................
cmstat:
        defb    $00	; ................
cmaflg:
        defb    $00	; ................
cmccnt:
        defb    $00	; ................
cmsflg:
        defb    $00	; ................
cmostp:
        defb    $00,$00	; ................
cmrprs:
        defb    $00,$00	; ................
cmptab:
        defb    $00,$00	; ................
L_5860:
        defb    $00,$00	; ................
cmdbuf:
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00	; ................
cmcptr:
        defb    $00,$00	; ................
cmdptr:
        defb    $00,$00	; ................
L_58EB:
        defb    $00,$00	; ................
L_58ED:
        defb    $00,$00	; ................
slshsn:
        defb    $00	; ................
slashc:
        defb    $00	; ................
slashn:
        defb    $00	; ...............&
ws_BasStk:
        defb    $00,$00	; ..............&.
ws_KerStk:
        defb    $00,$00	; ............&..K
L_58F6:
        defb    $00,$00	; ..........&..KER
L_58F8:
        defb    $00	; ........&..KERMI
L_58F9:
        defb    $00	; .......&..KERMIT
L_58FA:
        defb    $01	; ......&..KERMIT.
L_58FB:
        defb    $01	; .....&..KERMIT.L
timflg:
        defb    $00	; ....&..KERMIT.LO
timval:
        defb    $00,$00	; ...&..KERMIT.LOG
wrn8bl:
        defb    $00	; .&..KERMIT.LOG..
L_5900:
        defb    $26	; &..KERMIT.LOG...
L_5901:
        defb    $00	; ..KERMIT.LOG....
logflg:
        defb    $00	; .KERMIT.LOG.....
logfln:
        defb    $4b,$45,$52,$4d,$49,$54,$2e,$4c,$4f,$47,$00,$00,$00,$00,$00,$00	; KERMIT.LOG......
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00	; ...KERMIT.INI...
takopn:
        defb    $00	; ..KERMIT.INI....
L_5955:
        defb    $00	; .KERMIT.INI.....
L_5956:
        defb    $4b,$45,$52,$4d,$49,$54,$2e,$49,$4e,$49,$00	; KERMIT.INI......
L_5961:
        defb    $00	; ................
L_5962:
        defb    $00	; ................
L_5963:
        defb    $00	; ................
L_5964:
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
L_59E4:
        defb    $00	; ................
stbuff:
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
L_5A65:
        defb    $00,$00	; ................
L_5A67:
        defb    $00,$00,$00,$00	; ................
L_5A6B:
        defb    $00,$00	; ................
number:
        defb    $00,$00	; ................
strcnt:
        defb    $00	; ................
L_5A70:
        defb    $00	; ................
L_5A71:
        defb    $00	; ................
L_5A72:
        defb    $00	; ................
L_5A73:
        defb    $00	; ................
L_5A74:
        defb    $00	; ................
L_5A75:
        defb    $00	; ................
L_5A76:
        defb    $00,$00	; ................
L_5A78:
        defb    $00,$00	; ................
L_5A7A:
        defb    $00,$00	; ................
datptr:
        defb    $00,$00	; ................
L_5A7E:
        defb    $00,$00	; ................
pktptr:
        defb    $00,$00	; ................
L_5A82:
        defb    $00	; ................
curchk:
        defb    $00	; ................
L_5A84:
        defb    $00	; ................
czseen:
        defb    $00	; ................
pktnum:
        defb    $00	; ................
numpkt:
        defb    $00,$00	; ................
numrtr:
        defb    $00,$00	; ................
numtry:
        defb    $00	; ................
oldtry:
        defb    $00	; ................
state:
        defb    $00	; ................
L_5A8E:
        defb    $01	; ................
L_5A8F:
        defb    $00	; ................
L_5A90:
        defb    $00	; ................
L_5A91:
        defb    $00	; ................
L_5A92:
        defb    $00	; ................
remdat:
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
recpkt:
        defb    $00	; ................
L_5AEE:
        defb    $00,$00	; ................
L_5AF0:
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$0d,$00	; ................
L_5B54:
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00	; ................
L_5BB9:
        defb    $00	; ................
L_5BBA:
        defb    $00	; ................
temp2:
        defb    $00	; ................
L_5BBC:
        defb    $00	; ................
L_5BBD:
        defb    $00	; ................
L_5BBE:
        defb    $00	; ................
argblk:
        defb    $00	; ................
L_5BC0:
        defb    $00	; ................
L_5BC1:
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
remnam:
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
L_5C1B:
        defb    $00	; ...........N^^..
timout:
        defb    $00,$10	; ..........N^^...
ws_terminal:
        defb    $01	; ........N^^.....
escchr:
        defb    $1c	; .......N^^......
ws_baudrate:
        defb    $ff	; ......N^^.......
L_5C21:
        defb    $ff	; .....N^^........
dbgflg:
        defb    $00	; ....N^^........#
L_5C23:
        defb    $00	; ...N^^........##
L_5C24:
        defb    $01	; ..N^^........##1
L_5C25:
        defb    $01	; .N^^........##1.
parity:
        defb    $4e	; N^^........##1..
L_5C27:
        defb    $5e	; ^^........##1...
L_5C28:
        defb    $5e,$08	; ^........##1....
L_5C2A:
        defb    $05	; .......##1......
L_5C2B:
        defb    $00	; ......##1.......
L_5C2C:
        defb    $00	; .....##1........
L_5C2D:
        defb    $00	; ....##1.........
L_5C2E:
        defb    $00	; ...##1..........
L_5C2F:
        defb    $0d	; ..##1...........
L_5C30:
        defb    $0d	; .##1............
L_5C31:
        defb    $23	; ##1.............
L_5C32:
        defb    $23	; #1..............
L_5C33:
        defb    $31	; 1...............
L_5C34:
        defb    $00	; ................
L_5C35:
        defb    $00	; ................
wchopn:                 ; wildcard handler (1:open, 0:closed)
        defb    $00
filopn:                 ; file handle state (1:open, 0:closed)
        defb    $00
flnlen:                 ; filename length
        defb    $00
flnbuf:
        defb    $00
L_5C3A:
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
buffer128:
        defb    $00,$00,$00,$00,$00,$00	; ................
L_5C8F:
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
L_5D0A:
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00,$00,$00,$00,$00,$00	; ................
L_5D32:
        defb    $00	; ................

hndfil:
        defw    $0000
hnd_wc:
        defw    $0000
hndtak:
        defw    $0000
L_5D39:
        defw    $0000
nq_txbaud:
        defw    $0000
nq_rxbaud:
        defw    $0000

nq_xonxoff:
        defb    $00
nq_parity:
        defb    $00
L_5DCF:
        defm    $0b, $0a, $09, $08, $0c
L_5DD4:
        defm    $01, "7#"
L_5DD7:
        defm    $00
L_5DD8:
        defm    $00
L_5DD9:
        defm    $00
L_5DDA:
        defm    $00
L_5DDB:
        defm    $00
L_5DDC:
        defm    $00
        defm    $01, "2I"
L_5DE0:
        defm    $00
        defm    $00
L_5DE2:
        defm    $01, "2H"
L_5DE5:
        defm    $00
        defm    $01, "3@"
; 5DE9h
L_5DE9:
        defb    $00	; ................
L_5DEA:
        defb    $00,$00	; ................
L_5DEC:
        defb    $00	; ................
L_5DED:
        defb    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; ................
        defb    $00,$00,$00	; ................

